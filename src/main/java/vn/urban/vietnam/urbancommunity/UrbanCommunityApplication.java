package vn.urban.vietnam.urbancommunity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * エントリーポイント
 */
@SpringBootApplication
public class UrbanCommunityApplication {

	public static void main(String[] args) {
		SpringApplication.run(UrbanCommunityApplication.class, args);
	}

}
