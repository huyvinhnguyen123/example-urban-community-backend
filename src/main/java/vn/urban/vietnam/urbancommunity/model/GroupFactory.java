package vn.urban.vietnam.urbancommunity.model;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.GroupEntity;
import vn.urban.vietnam.urbancommunity.type.Avatar;
import vn.urban.vietnam.urbancommunity.type.Group;

import java.util.Objects;

/**
 * type グループ ファクトリークラス
 */
@Component
@RequiredArgsConstructor
public class GroupFactory {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** ファクトリ */
	private final AvatarFactory avatarFactory; // グループ生成器
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * GroupEntity → type Group
	 * @param entity レコード
	 * @return 生成結果
	 */
	public Group createOf(GroupEntity entity) {
		// 前提条件
		if (Objects.isNull(entity)) return null;
		// 参照テーブルのファクトリ実行
		Avatar avatar = avatarFactory.createOf(entity.getAvatar()); // グループのアバター

		// 値生成
		return Group.builder()
				.id(entity.getGroupId())
				.groupName(entity.getGroupName())
				.avatar(avatar)
				.build();
	}
}
