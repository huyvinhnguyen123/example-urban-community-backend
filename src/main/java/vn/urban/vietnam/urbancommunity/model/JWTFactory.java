package vn.urban.vietnam.urbancommunity.model;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.config.security.SecurityProperties;
import vn.urban.vietnam.urbancommunity.type.User;

import java.time.Instant;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

/**
 * type JWT ファクトリークラス
 */
@Component
@RequiredArgsConstructor
public class JWTFactory {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** 環境設定値 */
	private final SecurityProperties properties; // このプロジェクトのセキュリティ設定値
	private final Algorithm algorithm; // JWT 生成 アルゴリズム
	private final JWTVerifier verifier; // JWT 解析者

	/**
	 * User → JWT 文字列
	 * @param user レコード
	 * @return 生成結果
	 */
	public String encodeJWT(final User user) {
		// 前提条件
		if (Objects.isNull(user)) return "";
		// 有効期限の生成
		Instant now = Instant.now();
		Instant expiry = Instant.now().plus(properties.getTokenExpiration());

		// JWTの生成
		return JWT
				.create()
				.withIssuer(properties.getTokenIssuer()) // 発行者
				.withIssuedAt(Date.from(now)) // 発行日
				.withExpiresAt(Date.from(expiry)) // 有効期限
				.withSubject(user.getId().toString()) // ユーザー識別子 = ログインID
				// TODO:その他付け加える情報あれば
				.sign(algorithm); // JWT暗号化
	}

	/**
	 * JWT の複合化
	 * @param token JWTの文字列
	 * @return JWT複合化した情報
	 */
	public Optional<DecodedJWT> decodedJWT(final String token) {
		// try and errorにて 情報解析
		try {
			// JWT の複合化
			return Optional.of(verifier.verify(token));
		} catch(JWTVerificationException ex) {
			// 複合化に失敗した場合は空 返却
			return Optional.empty();
		}
	}
}
