package vn.urban.vietnam.urbancommunity.model;

/**
 * ユーザー権限 定義
 */
public enum UserRoleType {
	USER, ADMIN;
}
