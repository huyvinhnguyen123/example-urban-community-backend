package vn.urban.vietnam.urbancommunity.model;

import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.AvatarEntity;
import vn.urban.vietnam.urbancommunity.type.Avatar;

import java.util.Objects;

/**
 * type アバター ファクトリークラス
 */
@Component
public class AvatarFactory {
	/**
	 * AvatarEntity → type Avatar
	 * @param entity レコード
	 * @return 生成結果
	 */
	public Avatar createOf(AvatarEntity entity) {
		// 前提条件
		if (Objects.isNull(entity)) return null;

		// 値生成
		return Avatar.builder()
				.id(entity.getAvatarId())
				.avatarURL(entity.getAvatarURL())
				.build();
	}
}
