package vn.urban.vietnam.urbancommunity.model.value;

/**
 * ソート項目名 定義 共通インターフェイス
 */
public interface SortField {
	/**
	 * @return ソートする フィルード名取得
	 */
	public String getField();
}
