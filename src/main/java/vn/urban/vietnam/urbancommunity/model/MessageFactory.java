package vn.urban.vietnam.urbancommunity.model;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.MessageEntity;
import vn.urban.vietnam.urbancommunity.type.Group;
import vn.urban.vietnam.urbancommunity.type.Message;
import vn.urban.vietnam.urbancommunity.type.Profile;

import java.util.Objects;

/**
 * type メッセージ ファクトリークラス
 */
@Component
@RequiredArgsConstructor
public class MessageFactory {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** ファクトリ */
	private final ProfileFactory profileFactory; // プロフィール生成器
	private final GroupFactory groupFactory; // グループ生成器
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * MessageEntity → type Message
	 * @param entity レコード
	 * @return 生成結果
	 */
	public Message createOf(MessageEntity entity) {
		// 前提条件
		if (Objects.isNull(entity)) return null;
		// 参照テーブルのファクトリ実行
		Profile profile = profileFactory.createOf(entity.getProfile()); // プロフィール情報
		Group group = groupFactory.createOf(entity.getGroup()); // 送信したグループ

		// 値生成
		return createOf(entity, profile, group);
	}
	/**
	 * MessageEntity → type Message
	 * @param entity レコード
	 * @param sender 送信者
	 * @param group 送信先グループ
	 * @return 生成結果
	 */
	public Message createOf(MessageEntity entity, Profile sender, Group group) {
		// 前提条件
		if (Objects.isNull(entity)) return null;

		// 値生成
		return Message.builder()
				.id(entity.getGroupMessageId().getMessageId())
				.group(group)
				.sender(sender)
				.messageText(entity.getMessageText())
//				.attachment(entity.) TODO: 今後ファイルアップロードとかに対応したら考えよう
				.createdAt(entity.getCreatedAt())
				.updatedAt(entity.getUpdatedAt())
				.build();
	}
}
