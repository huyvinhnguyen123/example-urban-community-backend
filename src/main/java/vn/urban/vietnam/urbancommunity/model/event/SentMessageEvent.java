package vn.urban.vietnam.urbancommunity.model.event;

import lombok.Getter;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.MessageId;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

/**
 * メッセージ送信イベント
 */
@Getter
public class SentMessageEvent extends MessageEvent {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 送信者 */
	private UserId sender;
	/*********************************************
	 * コンストラクタ
	 *********************************************/
	/**
	 * メッセージ送信 イベントコンストラクタ
	 * @param source 発火した場所
	 * @param groupId 対象メッセージのグループ
	 * @param messageId 対象メッセージ
	 * @param sender 送信者
	 */
	public SentMessageEvent(Object source, GroupId groupId, MessageId messageId, final UserId sender) {
		super(source, groupId, messageId);
		this.sender = sender;
	}
}
