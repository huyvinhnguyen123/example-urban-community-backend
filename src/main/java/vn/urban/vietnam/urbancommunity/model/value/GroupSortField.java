package vn.urban.vietnam.urbancommunity.model.value;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * GroupEntityのソート項目
 */
@Getter
@RequiredArgsConstructor
public enum GroupSortField implements SortField {
	NAME("name")
	, LATEST_MESSAGE_AT("TODO:結合 かつ MAX ORDER BY ... JPQLの書き方依存する... どうやろうか")
	;
	/** JPQL query フィールド名 */
	private final String field;
}
