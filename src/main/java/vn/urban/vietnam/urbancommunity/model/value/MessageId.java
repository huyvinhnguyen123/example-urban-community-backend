package vn.urban.vietnam.urbancommunity.model.value;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.util.lang.MyStringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

/**
 * domain メッセージID の定義
 */
@Embeddable
@Getter
@EqualsAndHashCode
@NoArgsConstructor(staticName = "private") // 新規登録の時は null可能
@AllArgsConstructor(staticName = "of")
public class MessageId implements Serializable {
	/*********************************************
	 * 内部参照可能定数
	 *********************************************/
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyyMMddhhmmssSSS");
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 値 (not change value) */
	@Column(name = "message_id", updatable = false)
	private String value;
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * @return メッセージID 裁判
	 */
	public static MessageId create() { return MessageId.of(makeRandom(new Date())); } // 実行日に該当するランダム文字列の生成
	/**
	 * @return true:値 空 / false:それ以外
	 */
	public boolean isEmpty() { return MyStringUtils.isEmpty(value); }
	/**
	 * @return 文字列表現
	 */
	@Override
	public String toString() { return isEmpty() ? null : "" + value; }
	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * ランダム文字列の生成
	 * @param date 指定日時
	 * @return 指定日時に該当するランダム文字列
	 */
	private static String makeRandom(final Date date) {
		// 補完
		Date dateValue = Optional.ofNullable(date).orElse(new Date()); // 指定日指定なし
		String dateString = DATE_FORMAT.format(dateValue); // yyyyMMddhhmmssSSS ms秒まで文字列
		// 指定日時に該当するランダム文字列 (22文字) = 5文字のランダム文字列 + yyyyMMddhhmmssSSS(17文字)
		return MyStringUtils.generateRandomAlphaNumeric(5) + dateString;
	}
}
