package vn.urban.vietnam.urbancommunity.model;

import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.UserEntity;
import vn.urban.vietnam.urbancommunity.type.User;

import java.util.Objects;

/**
 * type ユーザー ファクトリークラス
 */
@Component
public class UserFactory {

	/**
	 * UserEntity → type User
	 * @param entity レコード
	 * @return 生成結果
	 */
	public User createOf(UserEntity entity) {
		// 前提条件
		if (Objects.isNull(entity)) return null;

		// 値生成
		return User.builder()
				.id(entity.getUserId())
				.email(entity.getEmail())
				.build();
	}
}
