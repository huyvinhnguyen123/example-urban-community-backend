package vn.urban.vietnam.urbancommunity.model.value;

import org.springframework.data.domain.Sort;

import java.util.Optional;

/**
 * Domain XXXSort 共通インターフェイス
 */
public interface DomainSort {
	/**
	 * @return ソート項目の取得
	 */
	public SortField getField();
	/**
	 * @return デフォルト ソート項目の取得
	 */
	public SortField getDefaultField();
	/**
	 * @return 昇順,降順の取得
	 */
	public Order getOrder();
	/**
	 * @return デフォルト 昇順,降順の取得(継承なし: 昇順)
	 */
	public default Order getDefaultOrder() { return Order.ASC; }
	/**
	 * @return JPA ソート形 取得
	 */
	public default Sort getSort() {
		// 入力補完
		Order order = Optional.ofNullable(getOrder()).orElseGet(this::getDefaultOrder); // 昇順,降順
		SortField sortField = Optional.ofNullable(getField()).orElseGet(this::getDefaultField); // ソートデフォルト項目
		// JPA ソート型へ
		return Sort.by(order.getDirection(), sortField.getField());
	}
}
