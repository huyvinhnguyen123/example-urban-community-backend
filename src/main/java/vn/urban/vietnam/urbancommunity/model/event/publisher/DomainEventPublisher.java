package vn.urban.vietnam.urbancommunity.model.event.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.model.event.DomainEvent;

/**
 * 共通 ドメインイベント発火器
 */
@Component
public class DomainEventPublisher implements ApplicationEventPublisherAware {
	/** 共通ドメインイベント発火器 */
	@Autowired
	private ApplicationEventPublisher publisher;

	/**
	 * ドメインイベント発火器 初期化
	 * @param applicationEventPublisher Spring ドメインイベント発火器
	 */
	@Override
	public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
		// ドメイン層からイベントの配送を受け付けるためstatic領域に 格納
		this.publisher = applicationEventPublisher;
	}

	/**
	 * 外部参照可能関数 ドメインイベント発火
	 * @param domainEvent 発火するイベント引数
	 */
	public void publish(DomainEvent domainEvent) { publisher.publishEvent(domainEvent); }
}
