package vn.urban.vietnam.urbancommunity.model;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.ProfileEntity;
import vn.urban.vietnam.urbancommunity.type.Avatar;
import vn.urban.vietnam.urbancommunity.type.Profile;
import vn.urban.vietnam.urbancommunity.type.User;

import java.util.Objects;
import java.util.Optional;

/**
 * type プロフィール ファクトリークラス
 */
@Component
@RequiredArgsConstructor
public class ProfileFactory {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** ファクトリ */
	private final UserFactory userFactory; // ユーザー生成器
	private final AvatarFactory avatarFactory; // アバター生成器
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * ProfileEntity → type Profile
	 * @param entity レコード
	 * @return 生成結果
	 */
	public Profile createOf(ProfileEntity entity) {
		// 前提条件
		if (Objects.isNull(entity)) return null;
		// 参照テーブルのファクトリ実行
		User user = userFactory.createOf(entity.getUser()); // ユーザー情報
		Optional<Avatar> avatar = Optional.ofNullable(entity.getAvatar()).map(avatarFactory::createOf); // アバター情報

		// 値生成
		return Profile.builder()
				.id(user.getId())
				.user(user)
				.avatar(avatar.orElse(null))
				.userName(entity.getUserName())
				.lastName(entity.getLastName())
				.firstName(entity.getFirstName())
				.location(entity.getLocation())
				.belongs(entity.getBelongs())
				.phone(entity.getPhone())
				.build();
	}
}
