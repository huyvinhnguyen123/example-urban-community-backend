package vn.urban.vietnam.urbancommunity.model;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.UserBelongGroupEntity;
import vn.urban.vietnam.urbancommunity.type.Group;
import vn.urban.vietnam.urbancommunity.type.Profile;
import vn.urban.vietnam.urbancommunity.type.UserBelongGroup;

import java.util.Objects;

/**
 * type ユーザーが所属するグループ ファクトリークラス
 */
@Component
@RequiredArgsConstructor
public class UserBelongGroupFactory {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** ファクトリ */
	private final ProfileFactory profileFactory; // プロフィール生成器
	private final GroupFactory groupFactory; // グループ生成器
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * UserBelongGroupEntity → type UserBelongGroup
	 * @param entity レコード
	 * @return 生成結果
	 */
	public UserBelongGroup createOf(UserBelongGroupEntity entity) {
		// 前提条件
		if (Objects.isNull(entity)) return null;
		// 参照テーブルのファクトリ実行
		Profile profile = profileFactory.createOf(entity.getProfile()); // プロフィール情報
		Group group = groupFactory.createOf(entity.getGroup()); // グループ情報

		// 値生成
		return UserBelongGroup.builder()
				.id(entity.getUserBelongGroupId())
				.profile(profile)
				.group(group)
				.build();
	}
}
