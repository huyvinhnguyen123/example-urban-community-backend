package vn.urban.vietnam.urbancommunity.model.value;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

import java.io.Serializable;

/**
 * Groupテーブル ソート値
 */
@Getter
public class GroupSort implements DomainSort, Serializable {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** ソートする項目 */
	private final GroupSortField field;
	/** 昇順, 降順 */
	private final Order order;
	/*********************************************
	 * コンストラクタ
	 *********************************************/
	@JsonCreator
	public GroupSort(final @JsonProperty("field") GroupSortField field, @JsonProperty("order") Order order) {
		this.field = field;
		this.order = order;
	}
	/*********************************************
	 * 実装関数
	 *********************************************/
	/**
	 * @return デフォルトソート項目
	 */
	@Override
	public SortField getDefaultField() { return GroupSortField.LATEST_MESSAGE_AT; }
	/**
	 * @return デフォルト 昇順降順 (降順)
	 */
	@Override
	public Order getDefaultOrder() { return Order.DESC; }
}
