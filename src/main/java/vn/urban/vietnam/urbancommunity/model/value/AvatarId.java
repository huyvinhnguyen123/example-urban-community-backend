package vn.urban.vietnam.urbancommunity.model.value;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.util.lang.MyStringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * domain アイコン画像ID の定義
 */
@Embeddable
@Getter
@EqualsAndHashCode
@NoArgsConstructor(staticName = "private") // 新規登録の時は null可能
@AllArgsConstructor(staticName = "of")
public class AvatarId implements Serializable {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 値 (not change value) */
	@Column(name = "avatar_id", updatable = false)
	private String value;
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * @return true:値 空 / false:それ以外
	 */
	public boolean isEmpty() { return MyStringUtils.isEmpty(value); }
	/**
	 * @return 文字列表現
	 */
	@Override
	public String toString() { return isEmpty() ? null : "" + value; }
}
