package vn.urban.vietnam.urbancommunity.model.event;

import lombok.Getter;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.MessageId;

/**
 * メッセージイベント
 */
@Getter
public class MessageEvent extends DomainEvent {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 対象メッセージのグループ */
	private GroupId groupId;
	/** 対象メッセージ */
	private MessageId messageId;
	/*********************************************
	 * コンストラクタ
	 *********************************************/
	/**
	 * メッセージイベントコンストラクタ
	 * @param source 発火した場所
	 * @param groupId 対象メッセージのグループ
	 * @param messageId 対象メッセージ
	 */
	public MessageEvent(final Object source, final GroupId groupId, final MessageId messageId) {
		super(source);
		this.groupId = groupId;
		this.messageId = messageId;
	}
}
