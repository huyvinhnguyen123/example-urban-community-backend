package vn.urban.vietnam.urbancommunity.model.value;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.util.lang.MyStringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * domain アイコン画像URL の定義
 */
@Embeddable
@Data
@EqualsAndHashCode
@NoArgsConstructor(staticName = "private") // 新規登録の時は null可能
@AllArgsConstructor(staticName = "of")
public class AvatarURL implements Serializable {
	/*********************************************
	 * 内部参照可能定数
	 *********************************************/
	private static final String URL_FORMAT = "TODO: www.dropbox.com への作るかどうか";
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 値 */
	@Column(name = "avatar_url")
	private String value;
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * @return true:値 空 / false:それ以外
	 */
	public boolean isEmpty() { return MyStringUtils.isEmpty(value); }
	/**
	 * @return 文字列表現
	 */
	@Override
	public String toString() { return isEmpty() ? null : "" + value; }
}
