package vn.urban.vietnam.urbancommunity.model.event;

import org.springframework.context.ApplicationEvent;

import java.time.LocalDateTime;

/**
 * 共通ドメインイベント
 */
public abstract class DomainEvent extends ApplicationEvent {
	/** 時間記録 */
	private final LocalDateTime occurredOn = LocalDateTime.now();
	/*********************************************
	 * コンストラクタ
	 *********************************************/
	/**
	 * 共通ドメイン イベントコンストラクタ
	 * @param source 発火した場所のクラス
	 */
	public DomainEvent(Object source) { super(source); }
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * @return 発火時間取得
	 */
	public final LocalDateTime occurredOn() { return occurredOn; }
}
