package vn.urban.vietnam.urbancommunity.model.value;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.util.lang.MyStringUtils;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.regex.Pattern;

/**
 * domain メールアドレス の定義
 */
@Embeddable
@Getter
@EqualsAndHashCode
@NoArgsConstructor(staticName = "private") // 新規登録の時は null可能
public class Email implements Serializable {
	/*********************************************
	 * 内部参照可能定数
	 *********************************************/
	/** メアド文字列制限 TODO:フロントだけでいいかw */
	private static final Pattern FORMAT = Pattern.compile("^[a-zA-Z0-9_+-]+(.[a-zA-Z0-9_+-]+)*@([a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]*\\.)+[a-zA-Z]{2,}$");
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 値 */
	@Column(name = "email", unique = true, nullable = false)
	private String value;
	/*********************************************
	 * コンストラクタ
	 *********************************************/
	public Email(final String value) {
		super();
		// 前提条件 → 該当しないパターンは値 空
		if (MyStringUtils.isEmpty(value)) return; // 空
		if (!FORMAT.matcher(value).find()) return; // メアドじゃない

		// 正常値 値設定
		this.value = value;
	}
	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * 型生成関数
	 * @param value 生成値
	 * @return 生成結果
	 */
	public static Email of(final String value) { return new Email(value); }
	/**
	 * @return true:値 空 / false:それ以外
	 */
	public boolean isEmpty() { return MyStringUtils.isEmpty(value); }
	/**
	 * @return 文字列表現
	 */
	@Override
	public String toString() { return isEmpty() ? null : "" + value; }
}
