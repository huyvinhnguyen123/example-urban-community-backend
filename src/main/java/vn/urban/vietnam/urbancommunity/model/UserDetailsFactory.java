package vn.urban.vietnam.urbancommunity.model;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.UserEntity;
import vn.urban.vietnam.urbancommunity.util.lang.MyStreamUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * type Spring Security UserDetails ファクトリークラス
 */
@Component
public class UserDetailsFactory {
	/** spring security 用の 権限 TODO:テーブル作る必要がる時に user entityへ反映 */
	private static final Set<UserRoleType> DEFAULT_ROLE_TYPES = new HashSet<UserRoleType>(){{ add(UserRoleType.USER); }};

	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * UserEntity → type UserDetails
	 * @param entity レコード
	 * @return 生成結果
	 */
	public UserDetails createOf(UserEntity entity) {
		// 前提条件
		if (Objects.isNull(entity)) return null;

		// User Tables -> UserDetails
		return new User(entity.getUserId().toString(), entity.getPassword(), toAuthorities(DEFAULT_ROLE_TYPES));
	}

	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * m_user_roleTypes -> User Authorities on SpringSecurity
	 * @param roleTypes ユーザー権限一覧
	 * @return User Authorities
	 */
	private List<SimpleGrantedAuthority> toAuthorities(Collection<UserRoleType> roleTypes) {
		// ユーザー権限一覧から SpringSecurityのユーザーの持つ権限一覧型へ変換
		return MyStreamUtils.toStream(roleTypes) // 権限情報 一覧 = ユーザー権限から作成
				.map(UserRoleType::toString) // 文字列にした
				.map(SimpleGrantedAuthority::new) // 型を変換した
				.collect(Collectors.toList()); // 一覧に変換したもの
	}
}
