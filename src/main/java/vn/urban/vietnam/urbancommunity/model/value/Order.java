package vn.urban.vietnam.urbancommunity.model.value;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;

/**
 * 共通利用 ASC,DESC 定義
 */
@Getter
@RequiredArgsConstructor
public enum Order {
	// 昇順
	ASC(Sort.Direction.ASC)
	// 降順
	, DESC(Sort.Direction.DESC)
	;
	/** JPA direction を持つ */
	private final Sort.Direction direction;
}
