package vn.urban.vietnam.urbancommunity.util.lang;

import java.security.SecureRandom;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * 独自文字列 ユーティリティ関数 クラス
 */
public class MyStringUtils {
	/**
	 * 文字 空判定
	 * @param charSequence 判定文字
	 * @return true:空 / false:それ以外
	 */
	public static boolean isEmpty(final CharSequence charSequence) {
		return charSequence == null || charSequence.length() == 0;
	}

	/**
	 * 英数字 ランダム文字列生成
	 * @param length 長さ
	 * @return 指定長の 英数字 ランダム文字列
	 */
	public static String generateRandomAlphaNumeric(final int length) {
		// 前提条件
		if (length <= 0) return "";

		// 利用文字 ASCII範囲–英数字(0-9、a-z、A-Z)
		final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		// ランダム生成器 初期化
		SecureRandom random = new SecureRandom();

		// 指定長文だけ文字列を生成
		return IntStream.rangeClosed(1, length)
				.map((i) -> random.nextInt(chars.length())) // ランダムに 利用文字を決める(利用文字のindex)
				.mapToObj(chars::charAt) // 指定長文だけ ランダム 文字 1文字ずつ
				.map(Object::toString) // 文字列方にして
				.collect(Collectors.joining()); // 先頭から結合した文字列 = ランダム文字列
	}
}
