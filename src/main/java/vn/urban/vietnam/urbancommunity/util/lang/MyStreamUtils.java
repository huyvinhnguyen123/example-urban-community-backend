package vn.urban.vietnam.urbancommunity.util.lang;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * Stream 周りの ユーティリティ関数 クラス
 */
public class MyStreamUtils {
	/**
	 * 安全な Collection -> Stream
	 * @param collection コレクション
	 * @return 安全な Collection -> Stream
	 */
	public static <T> Stream<T> toStream(Collection<T> collection) {
		// nullまたは空の時 空Streamを返却する安全策 を施す
		return MyCollectionUtils.isEmpty(collection) ? Stream.empty() : collection.stream();
	}
}
