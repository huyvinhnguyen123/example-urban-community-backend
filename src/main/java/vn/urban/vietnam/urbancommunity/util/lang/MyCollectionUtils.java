package vn.urban.vietnam.urbancommunity.util.lang;

import java.util.Collection;
import java.util.Map;

/**
 * Collection 周りの ユーティリティ関数 クラス
 */
public class MyCollectionUtils {
	/**
	 * Collection 空判定
	 * @param collection コレクション
	 * @return true: 空 / false: それ以外
	 */
	public static boolean isEmpty(Collection<?> collection) { return collection == null || collection.isEmpty(); }
	/**
	 * Map 空判定
	 * @param map マップ
	 * @return true: 空 / false: それ以外
	 */
	public static boolean isEmpty(Map<?, ?> map) { return map == null || map.isEmpty(); }
}
