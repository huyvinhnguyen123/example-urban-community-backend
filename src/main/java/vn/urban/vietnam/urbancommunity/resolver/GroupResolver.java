package vn.urban.vietnam.urbancommunity.resolver;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.service.MessageService;
import vn.urban.vietnam.urbancommunity.service.SeenMessageService;
import vn.urban.vietnam.urbancommunity.service.UserBelongGroupService;
import vn.urban.vietnam.urbancommunity.service.UserService;
import vn.urban.vietnam.urbancommunity.type.Group;
import vn.urban.vietnam.urbancommunity.type.Message;
import vn.urban.vietnam.urbancommunity.type.Profile;
import vn.urban.vietnam.urbancommunity.type.User;

import java.util.List;
import java.util.Optional;

/**
 * type Group フィールドリソルバー
 */
@Component
@RequiredArgsConstructor
public class GroupResolver implements GraphQLResolver<Group> {
	/*********************************************
	 * filed
	 *********************************************/
	/** services */
	private final UserService userService; // ユーザーサービス
	private final UserBelongGroupService userBelongGroupService; // ユーザー所属グループサービス
	private final MessageService messageService; // メッセージサービス
	private final SeenMessageService seenMessageService; // メッセージサービス

	/*********************************************
	 * type filed Query
	 **********************************************/
	/**
	 * type Group.latestMessage のフィールド (グループの最新メッセージ)
	 * @param group グループ情報
	 * @return type Group.latestMessageの値
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public Optional<Message> latestMessage(final Group group) {
		// グループの最新メッセージ 検索
		return messageService.findLatestMessageByGroup(group.getId());
	}
	/**
	 * type Group.belongMembers のフィールド (グループの所属メンバープロフィール一覧)
	 * @param group グループ情報
	 * @return type Group.belongMembers
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public List<Profile> belongMembers(final Group group) {
		// 指定グループに所属するメンバーの一覧 検索
		return userBelongGroupService.findBelongMembers(group.getId());
	}

	/**
	 * type Group.unreadCount のフィールド (未読数取得)
	 * @param group グループ情報
	 * @return type Group.unreadCount
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public int countOfUnread(final Group group) {
		// アクセスユーザー取得
		User me = userService.getCurrentUser();
		// 自分の返却グループの未読数
		return seenMessageService.countOfUnread(me.getId(), group.getId());
	}
}
