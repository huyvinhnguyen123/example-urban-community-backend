package vn.urban.vietnam.urbancommunity.resolver;

import graphql.kickstart.tools.GraphQLSubscriptionResolver;
import lombok.RequiredArgsConstructor;
import org.reactivestreams.Publisher;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.service.MessageService;
import vn.urban.vietnam.urbancommunity.service.UserBelongGroupService;
import vn.urban.vietnam.urbancommunity.service.UserService;
import vn.urban.vietnam.urbancommunity.type.Message;
import vn.urban.vietnam.urbancommunity.type.User;

/**
 * subscription graphql resolver
 *  ※ 認証情報は特殊 JWT検証するが 接続時にsession 利用するような感じ → MySubscriptionConnectionListener 参照
 */
@Component
@RequiredArgsConstructor
public class SubscriptionResolver implements GraphQLSubscriptionResolver {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** サービス */
	private final UserService userService; // ユーザーサービス
	private final UserBelongGroupService belongGroupService; // 所属グループサービス
	private final MessageService messageService; // メッセージサービス

	/*********************************************
	 * GraphQL subscription 一覧
	 *********************************************/
	/**
	 * 自身宛の 指定グループの 送信されたメッセージ購読
	 * @return 新着メッセージ 通知結果
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public Publisher<Message> sentMessage(final GroupId groupId) {
		// アクセスユーザー取得
		User me = userService.getCurrentUser();
		// 自身宛の新着メッセージ購読
		return messageService.sentMessage(me.getId(), groupId);
	}
}
