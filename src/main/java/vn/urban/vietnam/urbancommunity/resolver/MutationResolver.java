package vn.urban.vietnam.urbancommunity.resolver;

import graphql.kickstart.tools.GraphQLMutationResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.exception.MyBadCredentialsException;
import vn.urban.vietnam.urbancommunity.model.value.UserId;
import vn.urban.vietnam.urbancommunity.service.MessageService;
import vn.urban.vietnam.urbancommunity.service.ProfileService;
import vn.urban.vietnam.urbancommunity.service.UserService;
import vn.urban.vietnam.urbancommunity.type.AppendMessageInput;
import vn.urban.vietnam.urbancommunity.type.Message;
import vn.urban.vietnam.urbancommunity.type.Profile;
import vn.urban.vietnam.urbancommunity.type.User;

@Component
@RequiredArgsConstructor
public class MutationResolver implements GraphQLMutationResolver {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** サービス */
	private final UserService userService; // ユーザーサービス
	private final ProfileService profileService; // プロフィールサービス
	private final MessageService messageService; // メッセージサービス
	/** 認証用 */
	private final AuthenticationProvider authenticationProvider; // 認証コンテキスト

	/*********************************************
	 * GraphQL mutation 一覧
	 *********************************************/
	/**
	 * ID・PW ログイン
	 * @param email loginID
	 * @param password loginPW
	 * @return ログインしたユーザーのプロフィール
	 */
	@PreAuthorize("isAnonymous()") // UserSerive.isAnonymous にて権限チェック 未ログイン(匿名ユーザー)であれば誰でも
	public Profile login(String email, String password) {
		// SpringSecurityでの 認証するID・PWの型へ変換
		UsernamePasswordAuthenticationToken credentials = new UsernamePasswordAuthenticationToken(email, password);
		try {
			/**
			 * memo:
			 *  authenticationProvider.authenticate のところでID・PWの検証をしている
			 *      1. implements UserDetailServiceの実装クラスUserService loadUserByUsernameでemailからユーザー(UserDetails)を検索
			 *      2. UserDetailsのパスワード == 暗号化した password であるかチェック
			 *  にて ID・PWの検証実施
			 */
			// ID・PW認証を実行
			SecurityContextHolder.getContext().setAuthentication(this.authenticationProvider.authenticate(credentials));
			// 認証結果正常の場合 認証したユーザーの情報を取得
			User me = this.userService.getCurrentUser();
			if (me == null) throw new UsernameNotFoundException("メールアドレスまたはパスワードに誤りがあります。"); // 見つからない = ID,PW間違いにする
			// ユーザープロフィール取得
			return profileService.getProfile(me.getId());
		} catch (AuthenticationException ex) {
			// 認証エラーの場合
			throw new MyBadCredentialsException();
		}
	}

	/**
	 * メッセージ追加
	 * @param appendMessageInput 追加するメッセージ
	 * @return 追加結果
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること TODO:色々検証終わったら
	public Message appendMessage(final AppendMessageInput appendMessageInput) {
		// アクセスユーザー取得
		User me = userService.getCurrentUser();
		// メッセージ追加
		return messageService.create(me.getId(), appendMessageInput);
	}
}
