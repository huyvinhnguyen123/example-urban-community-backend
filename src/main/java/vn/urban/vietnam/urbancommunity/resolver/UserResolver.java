package vn.urban.vietnam.urbancommunity.resolver;

import graphql.kickstart.tools.GraphQLResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.service.UserService;
import vn.urban.vietnam.urbancommunity.type.User;

/**
 * type User フィールドリソルバー
 */
@Component
@RequiredArgsConstructor
public class UserResolver implements GraphQLResolver<User> {
	/*********************************************
	 * filed
	 *********************************************/
	/** services */
	private final UserService service; // ユーザーサービス

	/*********************************************
	 * type filed Query
	 **********************************************/
	/**
	 * type User.token のフィールド
	 * @param user ユーザー情報
	 * @return type User.tokenの値
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public String getToken(final User user) { return this.service.encodeJWT(user); }
}
