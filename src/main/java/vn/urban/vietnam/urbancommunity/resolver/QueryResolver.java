package vn.urban.vietnam.urbancommunity.resolver;

import graphql.kickstart.tools.GraphQLQueryResolver;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.GroupSort;
import vn.urban.vietnam.urbancommunity.service.MessageService;
import vn.urban.vietnam.urbancommunity.service.ProfileService;
import vn.urban.vietnam.urbancommunity.service.SeenMessageService;
import vn.urban.vietnam.urbancommunity.service.UserBelongGroupService;
import vn.urban.vietnam.urbancommunity.service.UserService;
import vn.urban.vietnam.urbancommunity.type.Group;
import vn.urban.vietnam.urbancommunity.type.Message;
import vn.urban.vietnam.urbancommunity.type.Profile;
import vn.urban.vietnam.urbancommunity.type.User;

import java.util.List;
import java.util.Optional;

@Component
@RequiredArgsConstructor
public class QueryResolver implements GraphQLQueryResolver {
	/*********************************************
	 * サービス
	 *********************************************/
	private final UserService userService; // ユーザーサービス
	private final ProfileService profileService; // プロフィールサービス
	private final UserBelongGroupService userBelongGroupService; // ユーザー所属グループサービス
	private final MessageService messageService; // メッセージ サービス
	private final SeenMessageService seenMessageService;

	// TODO: 開発用 パスワードエンコード
	private final PasswordEncoder passwordEncoder;
	/*********************************************
	 * GraphQL query 一覧
	 *********************************************/
	/**
	 * @return ログインユーザー情報取得 query
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public Profile getMe() {
		// アクセスユーザー取得
		User me = userService.getCurrentUser();
		// ユーザープロフィール取得
		return profileService.getProfile(me.getId());
	}

	/**
	 * 所属グループ一覧
	 * @param page 今のページ番号
	 * @param size 表示件数 / 1ページ
	 * @param sort ソートフィールド
	 * @return 所属グループ一覧 query
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public List<Group> getBelongGroups(final Integer page, final Integer size, final GroupSort sort) {
		// アクセスユーザー取得
		User me = userService.getCurrentUser();
		// 自信が所属する グループの一覧取得 TODO: ソート項目として 最新メッセージ(Groupの結合先メッセージのフィールドどうやってJPQLにするかDB作ってから考える)
		return userBelongGroupService.findBelongGroups(me.getId());
	}

	/**
	 * 所属グループ 取得
	 * @param groupId 取得グループ
	 * @return 所属グループ一覧 query
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public Optional<Group> getBelongGroup(final GroupId groupId) {
		// アクセスユーザー取得
		User me = userService.getCurrentUser();
		// 自信が所属する グループの一覧取得
		return userBelongGroupService.findBelongGroup(me.getId(), groupId);
	}

	/**
	 * 指定グループのメッセージ一覧
	 * @param groupId 指定グループ
	 * @param page 今のページ番号
	 * @param size 表示件数 / 1ページ
	 * @return 指定グループのメッセージ一覧
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public List<Message> getMessages(final GroupId groupId, final Integer page, final Integer size) {
		// 指定グループのメッセージ一覧取得 TODO: limit, offset
		return messageService.findMessages(groupId);
	}

	/**
	 * 指定グループの未読メッセージ数
	 * @param groupId 指定グループ
	 * @return 指定グループの未読メッセージ数
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること
	public int countOfUnread(final GroupId groupId) {
		// アクセスユーザー取得
		User me = userService.getCurrentUser();
		// 自身の指定グループの未読メッセージ数
		return seenMessageService.countOfUnread(me.getId(), groupId);
	}

	// TODO: 疎通確認用
//	@PreAuthorize("permitAll")
	public String hello() { return "Hello World"; }
	// TODO: パスワードの値を知りたいがためのやつ なので終わったら消す
//	@PreAuthorize("permitAll")
	public String passwordEncode(final String password) { return passwordEncoder.encode(password); }
	public boolean existEmail(final String email) { return userService.existEmail(email); }
}
