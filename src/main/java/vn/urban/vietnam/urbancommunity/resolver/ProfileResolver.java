package vn.urban.vietnam.urbancommunity.resolver;

import graphql.kickstart.tools.GraphQLResolver;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.type.Profile;

/**
 * type Profile フィールドリソルバー
 */
@Component
public class ProfileResolver implements GraphQLResolver<Profile> {
	// TODO: 友達サービス

	/**
	 * type Profile.friendCount のフィールド
	 * @param profile 対象プロフィール
	 * @return type Profile.friendCountの値
	 */
	@PreAuthorize("isAuthenticated()") // すでに認証済みであること TODO: JWT終わってから
	public int getFriendCount(final Profile profile) { return 0; } // TODO: スタブ
}
