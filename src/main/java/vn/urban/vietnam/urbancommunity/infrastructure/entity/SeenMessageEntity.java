package vn.urban.vietnam.urbancommunity.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.SeenMessageId;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * ユーザーの(グループの)メッセージ 閲覧履歴 エンティティクラス
 */
@Entity
@Table(name = "seen_message")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SeenMessageEntity {
	/*********************************************
	 * カラム
	 *********************************************/
	/** ID (複合キー: 自動生成しない) */
	@EmbeddedId
	private SeenMessageId seenMessageId;
	/** true: ユーザー閲覧 / false: 未読 */
	@Column(name = "saw", nullable = false)
	private boolean saw;
	/** 登録日 */
	@Getter
	@Embedded
	private CreatedAt createdAt;
	/** 更新日 */
	@Embedded
	private UpdatedAt updatedAt;
}
