package vn.urban.vietnam.urbancommunity.infrastructure.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.Email;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * ユーザーテーブル エンティティクラス
 */
@Entity
@Table(name = "urban_community_user") // PostgresSQL user 管理と同じ名前をさけ 独自の名前
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity {
	/*********************************************
	 * カラム
	 *********************************************/
	/** ユーザーID */
	@EmbeddedId
	private UserId userId;
	/** メアド */
	@Embedded
	private Email email;
	/** パスワード */
	@Column(name = "password", nullable = false)
	private String password;
	/** 登録日 */
	@Getter
	@Embedded
	private CreatedAt createdAt;
	/** 更新日 */
	@Embedded
	private UpdatedAt updatedAt;
	// TODO: 権限マスタ必要になったら下記対応
//	/** ユーザー権限 */
//	private Set<UserRoleType> roleTypes;
}
