package vn.urban.vietnam.urbancommunity.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.MessageEntity;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.GroupMessageId;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;

import java.util.List;

/**
 * メッセージテーブル アクセスリポジトリ
 */
@Repository
public interface MessageRepository extends JpaRepository<MessageEntity, GroupMessageId> {
	/**
	 * 指定グループのメッセージ一覧
	 * @param groupId 指定グループ
	 * @return 指定グループのメッセージ一覧
	 */
	public List<MessageEntity> findAllByGroupMessageIdGroupId(final GroupId groupId);

	/**
	 * 指定グループの最後のメッセージ
	 * @param groupId 指定グループ
	 * @return 最新メッセージ
	 */
	public MessageEntity findFirstByGroupMessageIdGroupIdOrderByUpdatedAtDesc(final GroupId groupId);
}
