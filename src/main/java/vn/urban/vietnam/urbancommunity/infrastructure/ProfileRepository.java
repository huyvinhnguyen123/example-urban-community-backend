package vn.urban.vietnam.urbancommunity.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.ProfileEntity;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

/**
 * プロフィールテーブル アクセスリポジトリ
 * TODO: DB作ってから extends JpaRepositoryへ … まずはモックアップのリポジトリ
 */
@Repository
public interface ProfileRepository extends JpaRepository<ProfileEntity, UserId> {
}
