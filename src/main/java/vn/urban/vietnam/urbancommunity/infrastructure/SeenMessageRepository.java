package vn.urban.vietnam.urbancommunity.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.SeenMessageEntity;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.SeenMessageId;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

import java.util.List;

/**
 * メッセージ 閲覧履歴 管理テーブル アクセスリポジトリ
 */
@Repository
public interface SeenMessageRepository extends JpaRepository<SeenMessageEntity, SeenMessageId> {
	/**
	 * 指定ユーザーの指定グループで 閲覧していないメッセージ 閲覧履歴一覧
	 * @param userId 指定ユーザー
	 * @param groupId 指定グループ
	 * @return 閲覧していない
	 */
	@Query("SELECT m FROM SeenMessageEntity m WHERE m.saw = FALSE AND m.seenMessageId.userId = :userId AND m.seenMessageId.groupId = :groupId")
	public List<SeenMessageEntity> findUnReads(final UserId userId, final GroupId groupId);
}
