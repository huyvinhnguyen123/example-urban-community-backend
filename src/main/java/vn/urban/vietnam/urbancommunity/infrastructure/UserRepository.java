package vn.urban.vietnam.urbancommunity.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.UserEntity;
import vn.urban.vietnam.urbancommunity.model.value.Email;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

import java.util.Optional;

/**
 * ユーザーテーブル アクセスリポジトリ
 */
@Repository
public interface UserRepository extends JpaRepository<UserEntity, UserId> {
	/**
	 * メアド から ユーザー検索
	 * @param email Email
	 * @return 該当ユーザー
	 */
	public Optional<UserEntity> findByEmail(Email email);
}
