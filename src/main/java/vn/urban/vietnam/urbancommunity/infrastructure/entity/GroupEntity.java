package vn.urban.vietnam.urbancommunity.infrastructure.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * グループテーブル エンティティクラス
 */
@Entity
@Table(name = "urban_community_group") // Group byのGroupと判別できないので 独自の名前
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GroupEntity {
	/*********************************************
	 * カラム
	 *********************************************/
	/** グループID */
	@EmbeddedId
	private GroupId groupId;
	/** グループ名 */
	@Column(name = "group_name", nullable = false)
	private String groupName;
	/** 登録日 */
	@Getter
	@Embedded
	private CreatedAt createdAt;
	/** 更新日 */
	@Embedded
	private UpdatedAt updatedAt;
	/*********************************************
	 * 参照テーブル
	 *********************************************/
	/** アバターテーブル 0 or 1*/
	@OneToOne
	@JoinColumn(name = "avatar_id", insertable = false, updatable = false)
	private AvatarEntity avatar;
}
