package vn.urban.vietnam.urbancommunity.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.GroupEntity;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;

/**
 * グループテーブル アクセスリポジトリ
 */
@Repository
public interface GroupRepository extends JpaRepository<GroupEntity, GroupId> {
}
