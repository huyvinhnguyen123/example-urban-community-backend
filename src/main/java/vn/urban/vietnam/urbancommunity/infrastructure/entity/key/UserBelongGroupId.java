package vn.urban.vietnam.urbancommunity.infrastructure.entity.key;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.GroupEntity;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.ProfileEntity;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.io.Serializable;
import java.util.Optional;

/**
 * domain ユーザー所属グループID の定義
 */
@Embeddable
@Data
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class UserBelongGroupId implements Serializable {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 対象ユーザー */
	@ManyToOne(fetch = FetchType.EAGER) // 該当カテゴリを一覧で取得 (LAZYにはしないで速さ重視)
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	@ToString.Exclude // 無限ループさせないため
	@EqualsAndHashCode.Exclude // 無限ループさせないため
	private ProfileEntity profile;
	/** 所属先グループ */
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "group_id", insertable = false, updatable = false)
	@ToString.Exclude // 無限ループさせないため
	@EqualsAndHashCode.Exclude // 無限ループさせないため
	private GroupEntity group;
	/*********************************************
	 * 独自 Getter
	 *********************************************/
	public UserId getUserId() { return Optional.ofNullable(this.getProfile()).map(ProfileEntity::getUserId).orElse(null); }
	public GroupId getGroupId() { return Optional.ofNullable(getGroup()).map(GroupEntity::getGroupId).orElse(null); }
}
