package vn.urban.vietnam.urbancommunity.infrastructure;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.UserBelongGroupEntity;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.UserBelongGroupId;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

import java.util.List;
import java.util.Optional;

/**
 * ユーザーが所属するグループテーブル アクセスリポジトリ
 */
@Repository
public interface UserBelongGroupRepository extends JpaRepository<UserBelongGroupEntity, UserBelongGroupId> {
	@Query("SELECT b FROM UserBelongGroupEntity b WHERE b.userBelongGroupId.profile.userId = :userId AND b.userBelongGroupId.group.groupId = :groupId")
	public Optional<UserBelongGroupEntity> findByUserBelongGroupId(final UserId userId, final GroupId groupId);
	/**
	 * 指定ユーザーの 所属するグループテーブル 一覧検索
	 * @param userId 指定ユーザー
	 * @return 指定ユーザーの 所属するグループテーブル 一覧
	 */
	public List<UserBelongGroupEntity> findAllByUserBelongGroupIdProfileUserId(final UserId userId);
	/**
	 * 指定グループの 所属するユーザー 一覧検索
	 * @param groupId 指定グループ
	 * @return 指定グループの 所属するユーザー 一覧
	 */
	public List<UserBelongGroupEntity> findAllByUserBelongGroupIdGroupGroupId(final GroupId groupId);
}
