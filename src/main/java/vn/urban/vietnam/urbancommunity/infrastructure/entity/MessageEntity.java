package vn.urban.vietnam.urbancommunity.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.GroupMessageId;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * メッセージテーブル エンティティクラス
 */
@Entity
@Table(name = "message")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MessageEntity {
	/*********************************************
	 * カラム
	 *********************************************/
	/** ID (複合キー: 自動生成しない) */
	@EmbeddedId
	private GroupMessageId groupMessageId;
	/** 送信者 */
	@Embedded
	@AttributeOverride(name = "value", column = @Column(name = "sender_id", nullable = false)) // CorporateName 側のカラム宣言内容の変更
	private UserId senderId;
	/** メッセージ */
	@Column(name = "message_text")
	private String messageText;
	/** 登録日 */
	@Getter
	@Embedded
	private CreatedAt createdAt;
	/** 更新日 */
	@Embedded
	private UpdatedAt updatedAt;

	/*********************************************
	 * 参照テーブル
	 *********************************************/
	/** 送信グループ */
	@ManyToOne(optional = false)
	@JoinColumn(name = "group_id", insertable = false, updatable = false)
	@ToString.Exclude // 無限ループさせないため
	@EqualsAndHashCode.Exclude // 無限ループさせないため
	private GroupEntity group;
	/** 送信者プロフィール */
	@ManyToOne(optional = false)
	@JoinColumn(name = "sender_id", referencedColumnName = "user_id", insertable = false, updatable = false)
	@ToString.Exclude // 無限ループさせないため
	@EqualsAndHashCode.Exclude // 無限ループさせないため
	private ProfileEntity profile;
	// TODO: 添付ファイルへのアクセス OneToOne (option true) 1(Message) - 0 or 1(Attachment)
}
