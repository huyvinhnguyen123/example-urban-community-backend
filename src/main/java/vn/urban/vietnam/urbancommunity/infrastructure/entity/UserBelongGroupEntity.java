package vn.urban.vietnam.urbancommunity.infrastructure.entity;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.UserBelongGroupId;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Optional;

/**
 * ユーザーが所属するグループテーブル エンティティクラス
 */
@Entity
@Table(name = "user_belong_group")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class UserBelongGroupEntity {
	/*********************************************
	 * カラム
	 *********************************************/
	/** ユーザー所属グループID (複合キー: 自動生成しない) */
	@EmbeddedId
	private UserBelongGroupId userBelongGroupId;
	/*********************************************
	 * 独自 Getter
	 *********************************************/
	public ProfileEntity getProfile() { return Optional.ofNullable(getUserBelongGroupId()).map(UserBelongGroupId::getProfile).orElse(null); }
	public GroupEntity getGroup() { return Optional.ofNullable(getUserBelongGroupId()).map(UserBelongGroupId::getGroup).orElse(null); }
}
