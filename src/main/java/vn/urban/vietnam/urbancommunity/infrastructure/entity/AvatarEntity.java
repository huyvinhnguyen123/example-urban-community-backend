package vn.urban.vietnam.urbancommunity.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.model.value.AvatarId;
import vn.urban.vietnam.urbancommunity.model.value.AvatarURL;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;

import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * アバターテーブル エンティティクラス
 */
@Entity
@Table(name = "avatar")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AvatarEntity {
	/*********************************************
	 * カラム
	 *********************************************/
	/** アバターID */
	@EmbeddedId
	private AvatarId avatarId;
	/** 保存先URL */
	@Embedded
	private AvatarURL avatarURL;
	/** 登録日 */
	@Getter
	@Embedded
	private CreatedAt createdAt;
	/** 更新日 */
	@Embedded
	private UpdatedAt updatedAt;
}
