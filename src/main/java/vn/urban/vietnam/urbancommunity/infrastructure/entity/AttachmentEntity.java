package vn.urban.vietnam.urbancommunity.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.GroupMessageId;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * メッセージテーブル エンティティクラス
 */
@Entity
@Table(name = "attachment")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AttachmentEntity {
	/*********************************************
	 * カラム
	 *********************************************/
	/** ID (複合キー: 自動生成しない) */
	@EmbeddedId
	private GroupMessageId groupMessageId;
	/** アップロード画像URL TODO:ファイルサーバー要件に従う てか下だけでいい説w */
	@Column(name = "thumb_url")
	private String thumbURL;
	/** アップロードファイルURL TODO:ファイルサーバー要件に従う */
	@Column(name = "file_url")
	private String fileURL;
	/** 登録日 */
	@Getter
	@Embedded
	private CreatedAt createdAt;
	/** 更新日 */
	@Embedded
	private UpdatedAt updatedAt;
}
