package vn.urban.vietnam.urbancommunity.infrastructure.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.Phone;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;
import vn.urban.vietnam.urbancommunity.model.value.UserId;
import vn.urban.vietnam.urbancommunity.model.value.UserName;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * プロフィールテーブル エンティティクラス
 */
@Entity
@Table(name = "profile")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProfileEntity {
	/*********************************************
	 * カラム
	 *********************************************/
	/** ユーザーID */
	@EmbeddedId
	private UserId userId;
	/** ユーザー名 */
	@Embedded
	private UserName userName;
	/** 名前 */
	@Column(name = "first_name")
	private String firstName;
	/** 苗字 */
	@Column(name = "last_name")
	private String lastName;
	/** 住所 */
	private String location;
	/** 所属会社 */
	private String belongs;
	/** 電話番号 */
	@Embedded
	private Phone phone;
	/** 登録日 */
	@Getter
	@Embedded
	private CreatedAt createdAt;
	/** 更新日 */
	@Embedded
	private UpdatedAt updatedAt;
	/*********************************************
	 * 参照テーブル
	 *********************************************/
	/** ユーザーテーブル 1-1 */
	@OneToOne(optional = false)
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private UserEntity user;
	/** アバターテーブル 0 or 1 */
	@OneToOne
	@JoinColumn(name = "avatar_id", insertable = false, updatable = false)
	private AvatarEntity avatar;
}
