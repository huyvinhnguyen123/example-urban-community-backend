package vn.urban.vietnam.urbancommunity.infrastructure.entity.key;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.MessageId;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

/**
 * domain ユーザーのグループのメッセージ閲覧履歴 ID の定義
 */
@Embeddable
@Getter
@EqualsAndHashCode
@NoArgsConstructor(staticName = "private") // 新規登録の時は null可能
@AllArgsConstructor(staticName = "of")
public class SeenMessageId implements Serializable {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** ユーザーID */
	@Embedded
	private UserId userId;
	/** 所属先グループID */
	@Embedded
	private GroupId groupId;
	/** メッセージID */
	@Embedded
	private MessageId messageId;
}
