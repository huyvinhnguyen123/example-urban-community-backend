package vn.urban.vietnam.urbancommunity.infrastructure.entity.key;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.MessageId;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import java.io.Serializable;

/**
 * domain グループのメッセージID の定義
 */
@Embeddable
@Getter
@EqualsAndHashCode
@NoArgsConstructor(staticName = "private") // 新規登録の時は null可能
@AllArgsConstructor(staticName = "of")
public class GroupMessageId implements Serializable {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 所属先グループID */
	@Embedded
	private GroupId groupId;
	/** メッセージID */
	@Embedded
	private MessageId messageId;
}
