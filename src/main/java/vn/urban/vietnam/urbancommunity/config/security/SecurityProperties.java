package vn.urban.vietnam.urbancommunity.config.security;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

import java.time.Duration;

/**
 * セキュリティ 設定値
 * TODO: applications.properties ファイルからの設定 するようにする
 */
@ConstructorBinding
@ConfigurationProperties(prefix = "security")
@Getter
@RequiredArgsConstructor
public class SecurityProperties {
	/**
	 * パスワードのハッシュ 暗号化の時の 暗号化する回数(ストレッチ回数) (指定回数の2乗)
	 */
	private final int passwordStrength = 10; // 10回に特に意味はない 確かデフォルト
	/**
	 * JWT暗号化文字列を生成するときの HMAC SHA-256の 秘密鍵
	 * TODO:特に意味はない → なんかいい感じの値に
	 */
	private final String tokenSecret = "urban-SHA256";
	/**
	 * JWT発行時の発行者名 (値の意味は特にない)
	 */
	private final String tokenIssuer = "urban-community";
	/**
	 * JWTの有効期限
	 * TODO: 取り敢えず 4時間
	 * TODO: 外部ファイルで設定?
	 */
	private final Duration tokenExpiration = Duration.ofHours(4); // 4 時間の意味は特にない
}
