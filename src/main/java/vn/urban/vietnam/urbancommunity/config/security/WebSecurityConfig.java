package vn.urban.vietnam.urbancommunity.config.security;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;
import java.util.Collections;

/**
 * Spring security としての基本的な WEB APIセキュリティ
 */
@Configuration
@EnableWebSecurity // Spring Security 有効
@EnableGlobalMethodSecurity(prePostEnabled = true) // MutationResolverなどのメソッドごとでのセキュリティ設定があるので 有効
@EnableConfigurationProperties(SecurityProperties.class) // セキュリティ関連の独自 外部設定ファイル値(プロパティ設定値) 有効
@RequiredArgsConstructor
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	/*********************************************
	 * 利用サービスなど
	 *********************************************/
	/** 認証・認可 作業に関する プロバイダー */
	private final AuthenticationProvider authenticationProvider;
	/** JWT認証ヘッダーの解析と認証フィルター */
	private final MyJWTFilter jwtFilter;
	/*********************************************
	 * 設定
	 *********************************************/
	/**
	 * 認証・認可 作業を集約した管理者(AuthenticationManager) に対しての設定
	 * @param auth 設定する 管理者生成器
	 */
	@Override
	protected void configure(AuthenticationManagerBuilder auth) {
		// 独自のPassword Encodeなどを設定する(SecurityConfig参照)するために設定
		auth.authenticationProvider(authenticationProvider);
	}

	/**
	 * WEB Http通信としての基本セキュリティ事項の登録
	 * @param http
	 * @throws Exception
	 */
	@Override
	protected void configure(HttpSecurity http) throws Exception{
		http
				.authorizeRequests().anyRequest().permitAll() // 認証済みリクエストの確認 (一旦全ページ)
				.and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // リクエスト毎に 認証を実施
				.and()
				.csrf().disable() // CSRF 対策機能を無効化
				.cors() // 独自の CORS設定読み込み
				.and()
				.addFilterBefore(jwtFilter, RequestHeaderAuthenticationFilter.class) // 共通事前認証filter登録: JWT認証ヘッダーによる認証フィルータの登録
		;
	}
	/*********************************************
	 * 設定
	 *********************************************/
	/**
	 * CORSOrigin 設定
	 * @return CORSOrigin 設定内容
	 */
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		// CORSOrigin の雑設定 (※ サンプルなため 本番環境などへの適用は注意)
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.setAllowedOriginPatterns(Arrays.asList("http://localhost:[*]", "Expo配信先サーバーかな...")); // 一旦だるいのでこんな感じで
		configuration.addAllowedHeader(CorsConfiguration.ALL); // どんな Headerでも一旦認めるぜの雑設定
		configuration.addAllowedMethod(CorsConfiguration.ALL); // どのリクエストに対しても 一旦認めるぜの雑設定
		configuration.setAllowCredentials(true);
		// 設定値反映
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}
}
