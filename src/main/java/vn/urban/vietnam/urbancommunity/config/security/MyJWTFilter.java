package vn.urban.vietnam.urbancommunity.config.security;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import vn.urban.vietnam.urbancommunity.exception.BadTokenException;
import vn.urban.vietnam.urbancommunity.model.PreAuthenticatedAuthenticationTokenFactory;
import vn.urban.vietnam.urbancommunity.service.UserService;
import vn.urban.vietnam.urbancommunity.util.lang.MyStringUtils;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Spring Security 独自filter 独自のJWT
 */
@Component
@RequiredArgsConstructor
public class MyJWTFilter extends OncePerRequestFilter {
	/*********************************************
	 * 内部参照可能定数
	 *********************************************/
	/** JWT の埋め込みされている HttpHeader Key名 */
	private static final String AUTHORIZATION_HEADER = "Authorization";
	/** JWT の埋め込みされている HttpHeader Valueの JWT以外の値 */
	private static final Pattern BEARER_PATTERN = Pattern.compile("^Bearer (.+?)$");
	/*********************************************
	 * Filed
	 *********************************************/
	/** User サービス */
	private final UserService userService;
	/** JWT事前認証結果 生成器 */
	private final PreAuthenticatedAuthenticationTokenFactory preAuthenticationTokenFactory;

	/*********************************************
	 * 独自の実装追加 (OncePerRequestFilter)
	 *********************************************/
	/**
	 * doFilter (認証していいものかの事前確認)のインターセプト
	 * @param request Http Request リクエスト内容
	 * @param response Http Response レスポンス内容
	 * @param filterChain 認証フィルターチェーン = JWT認証フィルター の内容を追加する先
	 */
	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
		try {
			// HttpRequest HeaderのJWTの値から 認証を実施し 認証結果を保存
			toToken(request) // HttpRequest HeaderのJWTの値から
					.map(this.userService::loadUserByToken) // JWTの値にて認証を実施し
					.map(userDetails -> this.preAuthenticationTokenFactory.create(userDetails, request)) // UserDetails -> PreAuthenticatedAuthenticationTokenへ (認可チェックできる形へ)
					.ifPresent(authentication -> SecurityContextHolder.getContext().setAuthentication(authentication)) // 認証できるJWTであったら 認証結果を保存
			;
		} catch (BadTokenException e) {
			// Token認証エラー ハンドリング ※ 今のgraphql-java-servletの使い方では resolverの前のエラーを拾えないため ハンドリング (query → JWT filter (ここ) → query resolver)
			SecurityContextHolder.clearContext(); // 認証・認可情報クリア
			// 401へ ※ 今のgraphql-java-servletの使い方では @ResponseStatus(HttpStatus.UNAUTHORIZED) ガン無視 500になるので レスポンスの形を無理やり指定
			((HttpServletResponse) response).sendError(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
		}
		// このJWT通信認証以外を 実施
		filterChain.doFilter(request, response);
	}

	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * Http Request -> JWT string
	 * @param request Http Request
	 * @return JWTの文字列
	 */
	private Optional<String> toToken(HttpServletRequest request) {
		// HttpRequestの JWT格納HttpHeader値から JWT格納HttpHeaderからJWT以外の値を 取り除いたJWTに該当する文字列 を抽出
		return Optional
				.ofNullable(request.getHeader(AUTHORIZATION_HEADER))// HttpRequestの JWT格納HttpHeader値から
				.filter((v) -> !MyStringUtils.isEmpty(v)) // 空文字列 以外のもので
				.map(BEARER_PATTERN::matcher) // JWT格納HttpHeaderからJWT以外の値を
				.filter(Matcher::find)
				.map(matcher -> matcher.group(1)); // 取り除いたJWTに該当する文字列 を抽出
	}
}
