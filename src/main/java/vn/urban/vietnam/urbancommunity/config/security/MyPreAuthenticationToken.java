package vn.urban.vietnam.urbancommunity.config.security;

import lombok.Builder;
import lombok.Getter;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;

import java.io.Serializable;

/**
 * UserDetailsのユーザー情報から得た権限値を保管し
 * SpringSecurityの @PreAuthorize("hasAuthority('ADMIN')") チェックなどの PreAuthorizeとして参照させるための値
 */
@Getter
public class MyPreAuthenticationToken extends PreAuthenticatedAuthenticationToken implements Serializable {
	/*********************************************
	 * コンストラクタ
	 *********************************************/
	@Builder
	public MyPreAuthenticationToken(UserDetails principal, WebAuthenticationDetails details) {
		super(principal, null, principal.getAuthorities());
		super.setDetails(details);
	}

	/*********************************************
	 * 実装
	 *********************************************/
	/**
	 * @return 外部API参照などによる APIキー認証 のやつは今回は自身で完結するので利用しない
	 */
	@Override
	public Object getCredentials() { return null; }
}
