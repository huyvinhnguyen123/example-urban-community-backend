package vn.urban.vietnam.urbancommunity.config;

import graphql.ExceptionWhileDataFetching;
import graphql.GraphQLError;
import graphql.kickstart.execution.error.GenericGraphQLError;
import graphql.kickstart.execution.error.GraphQLErrorHandler;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.util.lang.MyStreamUtils;

import java.util.List;
import java.util.stream.Collectors;

/**
 * resolver以降 で発火した エラー を GraphQLエラーへハンドリング的な
 */
@Component
public class MyGraphQLErrorHandler implements GraphQLErrorHandler {
	/**
	 * エラーハンドリング
	 * @param errors エラーの一覧
	 * @return ハンドリング後の変換結果
	 */
	@Override
	public List<GraphQLError> processErrors(List<GraphQLError> errors) {
		// 各エラー内容を 特定の形へ変換したものへ ハンドリング
		return MyStreamUtils.toStream(errors).map(this::unwrapError).collect(Collectors.toList());
	}

	/**
	 * エラー ハンドリング
	 * @param error 対象のエラー
	 * @return 変換結果
	 */
	private GraphQLError unwrapError(GraphQLError error) {
		// GraphQL 特定のエラーの場合
		if (error instanceof ExceptionWhileDataFetching) {
			ExceptionWhileDataFetching unwrappedError = (ExceptionWhileDataFetching) error;
			// 階層下げたエラー (エラーの詳細部の) の形へ変換
			return new GenericGraphQLError(unwrappedError.getException().getMessage());
		// それ以外
		} else {
			return error;
		}
	}
}
