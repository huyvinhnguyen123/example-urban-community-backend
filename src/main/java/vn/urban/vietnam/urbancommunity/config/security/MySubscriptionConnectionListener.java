package vn.urban.vietnam.urbancommunity.config.security;

import graphql.kickstart.execution.subscriptions.SubscriptionSession;
import graphql.kickstart.execution.subscriptions.apollo.ApolloSubscriptionConnectionListener;
import graphql.kickstart.execution.subscriptions.apollo.OperationMessage;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Component;
import vn.urban.vietnam.urbancommunity.exception.BadTokenException;
import vn.urban.vietnam.urbancommunity.service.UserService;
import vn.urban.vietnam.urbancommunity.util.lang.MyCollectionUtils;
import vn.urban.vietnam.urbancommunity.util.lang.MyStringUtils;

import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * subscription 通信接続時の 認証・認可処理用ハンドラー
 * example: https://github.com/philip-jvm/learn-spring-boot-graphql/blob/master/src/main/java/com/learn/graphql/config/security/AuthenticationConnectionListener.java
 */
@Component
@RequiredArgsConstructor
public class MySubscriptionConnectionListener implements ApolloSubscriptionConnectionListener {
	/*********************************************
	 * 内部参照可能定数
	 *********************************************/
	/** JWT の埋め込みされている OperationMessage.payload Key名 */
	private static final String AUTHORIZATION_PAYLOAD_KEY = "authorization";
	/** JWT の埋め込みされている OperationMessage.payload Valueの JWT以外の値 */
	private static final Pattern BEARER_PATTERN = Pattern.compile("^Bearer (.+?)$");
	/** subscription 通信セッション 認証値キー */
	private static final String SESSION_AUTHENTICATION_KEY = "urban-community/Authorization";
	/*********************************************
	 * フィールド
	 *********************************************/
	/** User サービス */
	private final UserService userService;
	/*********************************************
	 * ハンドリング
	 *********************************************/
	/**
	 * subscription 接続時ハンドリング
	 * @param session 通信セッション
	 * @param message subscription 通信内容
	 */
	@Override
	public void onConnect(SubscriptionSession session, OperationMessage message) {
		try {
			// subscription 通信内容 JWT取得し 認証を実施
			Optional<UserDetails> user = toToken(message).map(this.userService::loadUserByToken);
			// 存在しない → 後続終了
			if (!user.isPresent()) return;

			// Spring Security 認可検証値 by トークン生成
			PreAuthenticatedAuthenticationToken token = user.map(principal -> new PreAuthenticatedAuthenticationToken(principal, null, principal.getAuthorities())).get();

			// 接続時は subscription 通信セッションに認証情報を設定 だけ ※ SpringSecurityへの認可設定は subscription開始時
			// 理由としては 開始フレームがここ取らないで 2つのフレームが異なるスレッドで処理される可能性があるぽい → スレッドの実行処理順考慮かな
			session.getUserProperties().put(SESSION_AUTHENTICATION_KEY, token);
		} catch (BadTokenException e) {
			// Token認証エラー ハンドリング
			SecurityContextHolder.clearContext(); // 認証・認可情報クリア
		}
	}

	/**
	 * subscription 開始時ハンドリング
	 * @param session 通信セッション
	 * @param message subscription 通信内容
	 */
	@Override
	public void onStart(SubscriptionSession session, OperationMessage message) {
		// onConnectで 認証された情報取得
		Authentication authentication = (Authentication) session.getUserProperties().get(SESSION_AUTHENTICATION_KEY);
		// 共通 Spring Security contextの認証情報として設定
		SecurityContextHolder.getContext().setAuthentication(authentication);
	}

	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * subscription 通信 -> JWT string
	 * @param message subscription 通信内容
	 * @return JWTの文字列
	 */
	private Optional<String> toToken(final OperationMessage message) {
		// 前提条件
		if (message == null) return Optional.empty(); // 空対策

		// 通信内容取得
		Map<String, String> payload = (Map<String, String>) message.getPayload();
		if (MyCollectionUtils.isEmpty(payload)) return Optional.empty();

		// subscription 通信の JWT格納値から JWTに該当する文字列 を抽出
		return Optional
				.ofNullable(payload.get(AUTHORIZATION_PAYLOAD_KEY))// subscription 通信の JWT格納値から
				.filter((v) -> !MyStringUtils.isEmpty(v)) // 空文字列 以外のもので
				.map(BEARER_PATTERN::matcher) // JWT格納HttpHeaderからJWT以外の値を
				.filter(Matcher::find)
				.map(matcher -> matcher.group(1)); // 取り除いたJWTに該当する文字列 を抽出
	}
}
