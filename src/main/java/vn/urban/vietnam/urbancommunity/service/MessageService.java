package vn.urban.vietnam.urbancommunity.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Sinks;
import vn.urban.vietnam.urbancommunity.infrastructure.MessageRepository;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.MessageEntity;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.GroupMessageId;
import vn.urban.vietnam.urbancommunity.model.MessageFactory;
import vn.urban.vietnam.urbancommunity.model.event.SentMessageEvent;
import vn.urban.vietnam.urbancommunity.model.event.publisher.DomainEventPublisher;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.MessageId;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;
import vn.urban.vietnam.urbancommunity.model.value.UserId;
import vn.urban.vietnam.urbancommunity.type.AppendMessageInput;
import vn.urban.vietnam.urbancommunity.type.Message;
import vn.urban.vietnam.urbancommunity.util.lang.MyStreamUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * ユーザー所属グループ周りのサービス
 */
@Service
@RequiredArgsConstructor
public class MessageService {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** 利用 サービス */
	private final UserBelongGroupService userBelongGroupService; // ユーザー所属グループ
	private final ProfileService profileService; // プロフィールサービス
	private final GroupService groupService; // グループサービス
	private final SeenMessageService seenMessageService; // メッセージ閲覧履歴サービス
	/** イベント発火器 */
	private final DomainEventPublisher publisher;
	/** リポジトリ */
	private final MessageRepository repository;
	/** ファクトリ */
	private final MessageFactory factory;
	/** 内部利用変数 */
	private final Sinks.Many<Message> messageSink = Sinks.many().multicast().directBestEffort(); // メッセージ イベント管理者
	private final Flux<Message> messageFlux = messageSink.asFlux(); // メッセージ イベント購読者

	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * 指定グループのメッセージ一覧 検索
	 * @param groupId 検索 グループ
	 * @return 指定グループのメッセージ一覧
	 */
	public List<Message> findMessages(final GroupId groupId) {
		// 指定グループのメッセージ一覧
		return MyStreamUtils.toStream(repository.findAllByGroupMessageIdGroupId(groupId)).map(factory::createOf).collect(Collectors.toList());
	}

	/**
	 * 指定グループの最新メッセージ検索
	 * @param groupId グループ
	 * @return
	 */
	public Optional<Message> findLatestMessageByGroup(final GroupId groupId) {
		// 指定グループの最新メッセージ検索 (空 = グループにメッセージがまだない 許可)
		return Optional.ofNullable(repository.findFirstByGroupMessageIdGroupIdOrderByUpdatedAtDesc(groupId)).map(factory::createOf);
	}

	/**
	 * メッセージ追加
	 * @param sender 送信者
	 * @param input 追加メッセージ
	 * @return
	 */
	@Transactional(rollbackOn = Exception.class)
	public Message create(final UserId sender, final AppendMessageInput input) {
		// 前提条件
		if (!isBelongMember(sender, input.getGroupId())) throw new IllegalArgumentException("グループにいるその人？"); // ユーザー所属グループであること TODO: 404 存在しない

		// 新規登録実行 TODO: 添付ファイルのアップロードとかは今後
		MessageEntity entity = repository.saveAndFlush(buildCreate(sender, input)); // メッセージ登録
		// 新規登録値整形
		Message result = factory.createOf(entity, profileService.getProfile(sender), groupService.findById(input.getGroupId()).orElse(null));
		// 新着メッセージ通知
		sentMessage(result);
		// 結果返却
		return result;
	}

	/**
	 * 指定ユーザーの指定購読するグループ(部屋)への最新メッセージ通知
	 * @param user ユーザー
	 * @param subscribedGroup 購読するグループ(部屋)
	 * @return メッセージ通知者
	 */
	public Flux<Message> sentMessage(final UserId user, final GroupId subscribedGroup) {
		// 前提条件
		if (subscribedGroup == null || subscribedGroup.isEmpty()) return Flux.empty(); // 購読するグループの指定なし → 空(グループに所属していない とか...)
		// TODO: バリデーションとして購読するグループに所属しているなどは除く

		// 指定ユーザーの指定グループ(部屋)への最新メッセージ通知
		return messageFlux
				.filter(m -> !m.getSender().getId().equals(user)) // 自身が送信したメッセージ以外
				.filter(m -> subscribedGroup.equals(m.getGroup().getId())) // 指定グループのもの
				.filter(m -> !seenMessageService.isSaw(user, m.getGroup().getId(), m.getId())) // 閲覧済以外
				;
	}
	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * 新規登録値 生成
	 * @param senderId 送信者
	 * @param input 追加メッセージ
	 * @return 新規登録値
	 */
	private MessageEntity buildCreate(final UserId senderId, final AppendMessageInput input) {
		// 新規登録 メッセージID 採番
		MessageId messageId = MessageId.create();
		// 新規登録値 生成
		return MessageEntity
				.builder()
				.groupMessageId(GroupMessageId.of(input.getGroupId(), messageId)) // 複合キー
				.senderId(senderId)
				.messageText(input.getMessageText())
				.createdAt(CreatedAt.create())
				.updatedAt(UpdatedAt.create())
				.build();
	}

	/**
	 * メッセージ通知
	 * @param message 通知メッセージ
	 */
	private void sentMessage(final Message message) {
		// メッセージ送信イベント発火
		SentMessageEvent event = new SentMessageEvent(this, message.getGroup().getId(), message.getId(), message.getSender().getId());
		publisher.publish(event);
		// メッセージ通知
		messageSink.emitNext(message, (signalType, emitResult) -> emitResult == Sinks.EmitResult.FAIL_NON_SERIALIZED);
	}

	/**
	 * 指定ユーザーが指定グループの所属メンバーである判定
	 * @param userId 指定ユーザー
	 * @param groupId 指定グループ
	 * @return true: 所属メンバー / false: それ以外(所属メンバーでないかグループないか)
	 */
	private boolean isBelongMember(final UserId userId, final GroupId groupId) {
		return userBelongGroupService.isBelongMember(userId, groupId);
	}
}
