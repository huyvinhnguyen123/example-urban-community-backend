package vn.urban.vietnam.urbancommunity.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.urban.vietnam.urbancommunity.infrastructure.UserBelongGroupRepository;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.UserBelongGroupEntity;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.UserBelongGroupId;
import vn.urban.vietnam.urbancommunity.model.GroupFactory;
import vn.urban.vietnam.urbancommunity.model.ProfileFactory;
import vn.urban.vietnam.urbancommunity.model.UserBelongGroupFactory;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.UserId;
import vn.urban.vietnam.urbancommunity.type.Group;
import vn.urban.vietnam.urbancommunity.type.Profile;
import vn.urban.vietnam.urbancommunity.type.UserBelongGroup;
import vn.urban.vietnam.urbancommunity.util.lang.MyStreamUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * ユーザー所属グループ周りのサービス
 */
@Service
@RequiredArgsConstructor
public class UserBelongGroupService {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** リポジトリ */
	private final UserBelongGroupRepository repository;
	/** ファクトリ */
	private final UserBelongGroupFactory factory; // ユーザー所属グループ 生成器
	private final ProfileFactory profileFactory; // プロフィール生成器
	private final GroupFactory groupFactory; // グループ生成器

	/*********************************************
	 * 外部参照可能関数 (Domain Service)
	 *********************************************/
	/**
	 * 指定ユーザーの所属グループ 検索
	 * @param userId ユーザー
	 * @param groupId グループ
	 * @return 検索結果
	 */
	public Optional<UserBelongGroup> findById(final UserId userId, final GroupId groupId) {
		// 指定ユーザーの所属グループ 検索
		return repository.findByUserBelongGroupId(userId, groupId).map(factory::createOf);
	}
	/**
	 * 指定ユーザーが指定所属グループ 存在する判定
	 * @param userId ユーザー
	 * @param groupId グループ
	 * @return true: 存在 / false: それ以外
	 */
	public boolean isBelongMember(final UserId userId, final GroupId groupId) {
		// 指定ユーザーの所属グループ 値存在
		return findById(userId, groupId).isPresent();
	}

	/**
	 * 指定ユーザーが所属する グループ一覧取得
	 * @param userId 対象
	 * @return 取得結果
	 */
	public List<Group> findBelongGroups(final UserId userId) {
		// 前提条件
		if (userId == null || userId.isEmpty()) return new ArrayList<>();

		// 指定ユーザーが所属する グループ一覧検索
		return MyStreamUtils.toStream(repository.findAllByUserBelongGroupIdProfileUserId(userId))
				.map(UserBelongGroupEntity::getGroup)
				.map(groupFactory::createOf) // グループの形で
				.collect(Collectors.toList()); // 一覧返却
	}

	/**
	 * 指定ユーザーが所属する グループ 検索
	 * @param userId 対象ユーザー
	 * @param groupId 対象グループ
	 * @return 取得結果
	 */
	public Optional<Group> findBelongGroup(final UserId userId, final GroupId groupId) {
		// 前提条件
		if (userId == null || userId.isEmpty()) return Optional.empty();
		if (groupId == null || groupId.isEmpty()) return Optional.empty();
		// 指定ユーザーが所属するグループ
		return repository.findByUserBelongGroupId(userId, groupId)
				.map(UserBelongGroupEntity::getGroup)
				.map(groupFactory::createOf);
	}

	/**
	 * 指定グループに所属する ユーザー一覧 取得
	 * @param groupId 対象
	 * @return 取得結果
	 */
	public List<Profile> findBelongMembers(final GroupId groupId) {
		// 前提条件
		if (groupId == null) return new ArrayList<>();
		if (groupId.isEmpty()) return new ArrayList<>();

		// 指定グループに所属する ユーザー一覧 検索
		return MyStreamUtils.toStream(repository.findAllByUserBelongGroupIdGroupGroupId(groupId))
				.map(UserBelongGroupEntity::getProfile)
				.map(profileFactory::createOf)  // メンバーのプロフィールの形で
				.collect(Collectors.toList()); // 一覧返却
	}
}
