package vn.urban.vietnam.urbancommunity.service;

import lombok.RequiredArgsConstructor;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import vn.urban.vietnam.urbancommunity.infrastructure.SeenMessageRepository;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.SeenMessageEntity;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.SeenMessageId;
import vn.urban.vietnam.urbancommunity.model.event.SentMessageEvent;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.model.value.MessageId;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;
import vn.urban.vietnam.urbancommunity.model.value.UserId;
import vn.urban.vietnam.urbancommunity.type.Profile;
import vn.urban.vietnam.urbancommunity.util.lang.MyStreamUtils;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

/**
 * メッセージ 閲覧履歴 管理 サービス
 */
@Service
@RequiredArgsConstructor
public class SeenMessageService {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** 利用 サービス */
	private final UserBelongGroupService userBelongGroupService; // ユーザー所属グループ
	/** リポジトリ */
	private final SeenMessageRepository repository; // リポジトリ

	/*********************************************
	 * 外部参照可能関数
	 *********************************************/
	/**
	 * 指定ユーザーの指定グループでの 未読数
	 * @param userId 指定ユーザー
	 * @param groupId 指定グループ
	 * @return 未読数
	 */
	public int countOfUnread(final UserId userId, final GroupId groupId) {
		// 前提条件
		if (userId == null || userId.isEmpty()) return 0; // 引数空対策
		if (groupId == null || groupId.isEmpty()) return 0; // 引数空対策
		// 未読数カウント
		return repository.findUnReads(userId, groupId).size();
	}

	/**
	 * 閲覧済 確認
	 * @param userId 対象ユーザー
	 * @param groupId 対象メッセージのグループ
	 * @param messageId 対象メッセージ
	 * @return true: 閲覧済 / false: それ以外
	 */
	public boolean isSaw(final UserId userId, final GroupId groupId, final MessageId messageId) {
		// 前提条件
		if (userId == null || userId.isEmpty()) return false; // 引数空対策
		if (groupId == null || groupId.isEmpty()) return false; // 引数空対策
		if (messageId == null || messageId.isEmpty()) return false; // 引数空対策
		// 閲覧済確認 (なければ それ以外)
		return repository.findById(SeenMessageId.of(userId, groupId, messageId)).map(SeenMessageEntity::isSaw).orElse(false);
	}


	/**
	 * 指定ユーザーが 指定グループのメッセージ閲覧した へ更新
	 * @param userId 更新ユーザー
	 * @param groupId 閲覧したメッセージのグループ
	 * @return true: 正常完了 / false: それ以外
	 */
	@Transactional(rollbackOn = Exception.class)
	public boolean read(final UserId userId, final GroupId groupId) {
		// 前提条件 引数不正
		if (userId == null || userId.isEmpty()) return false;
		if (groupId == null || groupId.isEmpty()) return false;

		// 指定ユーザーの 指定グループの 未読メッセージ一覧 検索
		List<SeenMessageEntity> entities = repository.findUnReads(userId, groupId);
		// 未読メッセージ全て 閲覧へ 更新値生成
		List<SeenMessageEntity> saveEntities = MyStreamUtils.toStream(entities).map(e -> buildUpdate(e, true)).collect(Collectors.toList());
		// 更新実行
		repository.saveAllAndFlush(saveEntities);
		// 正常完了
		return true;
	}

	/**
	 * メッセージ送信 イベントハンドラー
	 * @param event メッセージ送信イベント引数
	 */
	@Transactional(rollbackOn = Exception.class)
	@EventListener
	@Async
	public void onSentMessage(final SentMessageEvent event) {
		// 前提条件 必要な情報不足 何もしない
		if (event == null) return;
		if (event.getSender() == null || event.getSender().isEmpty()) return;
		if (event.getGroupId() == null || event.getGroupId().isEmpty()) return;
		if (event.getMessageId() == null || event.getMessageId().isEmpty()) return;

		// メッセージ購読者一覧 検索 (送信者も含むグループ所属メンバー全員)
		List<UserId> members = findBelongMembers(event.getGroupId());

		// メッセージ購読者(所属メンバー)ごとに メッセージ閲覧履歴登録値生成
		List<SeenMessageEntity> entities = MyStreamUtils.toStream(members) // メッセージ購読者(所属メンバー)ごとに
				.map(m -> buildCreate(m, event.getGroupId(), event.getMessageId(), event.getSender().equals(m))) // メッセージ閲覧履歴登録値生成 (送信者:閲覧 / それ以外:未読)
				.collect(Collectors.toList());
		// 新規登録実行
		repository.saveAllAndFlush(entities);
	}
	/*********************************************
	 * 内部参照可能関数
	 *********************************************/
	/**
	 * グループメンバー検索
	 * @param groupId 指定グループ
	 * @return 検索結果
	 */
	private List<UserId> findBelongMembers(final GroupId groupId) {
		// グループの所属メンバー検索し 自身以外の結果一覧返却
		return MyStreamUtils.toStream(userBelongGroupService.findBelongMembers(groupId)) // 自身以外のグループメンバー検索
				.map(Profile::getId) // メンバーIDにする
				.collect(Collectors.toList()); // 結果一覧返却
	}
	/*********************************************
	 * 内部参照可能関数 (factory)
	 *********************************************/
	/**
	 * 新規登録値生成
	 * @param userId 対象ユーザー
	 * @param groupId 対象グループ
	 * @param messageId 対象メッセージ
	 * @param saw true:閲覧 / false:未読
	 * @return 新規登録値
	 */
	private SeenMessageEntity buildCreate(final UserId userId, final GroupId groupId, final MessageId messageId, final boolean saw) {
		return buildCreate(SeenMessageId.of(userId, groupId, messageId), saw);
	}
	/**
	 * 新規登録値生成
	 * @param id 対象
	 * @param saw true:閲覧 / false:未読
	 * @return 新規登録値
	 */
	private SeenMessageEntity buildCreate(final SeenMessageId id, final boolean saw) {
		// 指定閲覧状態で 新規登録値生成
		return SeenMessageEntity
				.builder()
				.seenMessageId(id)
				.saw(saw)
				.createdAt(CreatedAt.create())
				.updatedAt(UpdatedAt.create())
				.build();
	}

	/**
	 * 更新値生成
	 * @param value 対象値
	 * @param saw true:閲覧 / false:未読
	 * @return 更新値
	 */
	private SeenMessageEntity buildUpdate(final SeenMessageEntity value, final boolean saw) {
		// 指定閲覧状態で 更新値生成
		return SeenMessageEntity
				.builder()
				.seenMessageId(value.getSeenMessageId()) // ID = 継承
				.saw(saw) // 閲覧状態 = 更新
				.createdAt(value.getCreatedAt()) // 登録日時 = 継承
				.updatedAt(UpdatedAt.create()) // 更新日時 = 更新
				.build();
	}
}
