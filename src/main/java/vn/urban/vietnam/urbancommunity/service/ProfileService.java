package vn.urban.vietnam.urbancommunity.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.urban.vietnam.urbancommunity.exception.UserNotFoundException;
import vn.urban.vietnam.urbancommunity.infrastructure.ProfileRepository;
import vn.urban.vietnam.urbancommunity.model.ProfileFactory;
import vn.urban.vietnam.urbancommunity.model.value.UserId;
import vn.urban.vietnam.urbancommunity.type.Profile;

import java.util.Optional;

/**
 * プロフィール周りのサービス
 */
@Service
@RequiredArgsConstructor
public class ProfileService {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** リポジトリ */
	private final ProfileRepository repository;
	/** ファクトリ */
	private final ProfileFactory factory;
	/*********************************************
	 * 外部参照可能関数 (Domain Service)
	 *********************************************/
	/**
	 * 指定ユーザー プロフィール取得 (存在しない場合エラー)
	 * @param userId 対象
	 * @return 取得結果
	 */
	public Profile getProfile(final UserId userId) {
		// 指定ユーザーのプロフィール取得
		return findById(userId).orElseThrow(() -> new UserNotFoundException(userId));
	}
	/**
	 * 指定ユーザー プロフィール検索
	 * @param userId 検索ユーザー
	 * @return 検索結果
	 */
	public Optional<Profile> findById(final UserId userId) {
		// 検索実行
		return repository.findById(userId).map(factory::createOf); // レコード → 結果の形
	}
}
