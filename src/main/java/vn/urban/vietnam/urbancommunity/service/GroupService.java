package vn.urban.vietnam.urbancommunity.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import vn.urban.vietnam.urbancommunity.infrastructure.GroupRepository;
import vn.urban.vietnam.urbancommunity.model.GroupFactory;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;
import vn.urban.vietnam.urbancommunity.type.Group;
import vn.urban.vietnam.urbancommunity.util.lang.MyCollectionUtils;
import vn.urban.vietnam.urbancommunity.util.lang.MyStreamUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * グループ周りのサービス
 */
@Service
@RequiredArgsConstructor
public class GroupService {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** サービス */
	/** リポジトリ */
	private final GroupRepository repository;
	/** ファクトリ */
	private final GroupFactory factory;
	/*********************************************
	 * 外部参照可能関数 (Domain Service)
	 *********************************************/
	/**
	 * 指定グループの一覧 検索
	 * @param groupIds 指定グループの一覧
	 * @return 検索結果
	 */
	public List<Group> findAll(final List<GroupId> groupIds) {
		// 前提条件
		if (MyCollectionUtils.isEmpty(groupIds)) return new ArrayList<>();

		// 指定グループの一覧検索
		return MyStreamUtils.toStream(repository.findAllById(groupIds)).map(factory::createOf).collect(Collectors.toList());
	}
	/**
	 * 指定グループ 検索
	 * @param groupId 検索グループ
	 * @return 検索結果
	 */
	public Optional<Group> findById(final GroupId groupId) {
		// 検索実行
		return repository.findById(groupId).map(factory::createOf); // レコード → 結果の形
	}
}
