package vn.urban.vietnam.urbancommunity.service;

import com.auth0.jwt.interfaces.DecodedJWT;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import vn.urban.vietnam.urbancommunity.exception.BadTokenException;
import vn.urban.vietnam.urbancommunity.infrastructure.UserRepository;
import vn.urban.vietnam.urbancommunity.model.JWTFactory;
import vn.urban.vietnam.urbancommunity.model.UserDetailsFactory;
import vn.urban.vietnam.urbancommunity.model.UserFactory;
import vn.urban.vietnam.urbancommunity.model.value.Email;
import vn.urban.vietnam.urbancommunity.model.value.UserId;
import vn.urban.vietnam.urbancommunity.type.User;
import vn.urban.vietnam.urbancommunity.util.lang.MyStringUtils;

import javax.transaction.Transactional;
import java.util.Optional;

/**
 * ユーザー周りのサービス
 */
@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {
	/*********************************************
	 * フィールド変数
	 *********************************************/
	/** リポジトリ */
	private final UserRepository repository;
	/** ファクトリ */
	private final UserFactory userFactory; // GraphQL type User ファクトリー
	private final UserDetailsFactory userDetailsFactory; // Spring Security type UserDetails ファクトリー
	private final JWTFactory jwtFactory; // JWT ファクトリー

	/*********************************************
	 * 実装関数 (認証機能)
	 *********************************************/
	/**
	 * login ID から 認可ユーザー情報 を読み込み
	 * @param email login id = emailとしているので emailの文字列
	 * @return 該当する 認証ユーザー情報
	 * @throws UsernameNotFoundException 指定ユーザー(メアド)がいないエラー
	 */
	@Override
	@Transactional // 認証は認証の間でのトランザクション管理設定にする
	public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {
		// 前提条件
		if (!existEmail(email)) throw new UsernameNotFoundException(""); // メアドなし
		// 指定 email(loginId)に該当する ユーザー情報を検索し 認証ユーザー情報 へ変換する
		return repository.findByEmail(Email.of(email)) // 指定 loginIdに該当する ユーザー情報を検索
				.map(this.userDetailsFactory::createOf) // 検索結果 を login ID に該当する 認証ユーザー情報へ
				// TODO: 独自例外へ 存在しないパターンを共通化
				.orElseThrow(() -> new UsernameNotFoundException("メールアドレスまたはパスワードに誤りがあります。")); // 検索結果 見つからない場合はエラー
	}
	/**
	 * JWT から 認可ユーザー情報 を読み込み (JWTログイン)
	 * @param token 対象となるJWT文字列
	 * @return JWT に該当する 認可ユーザー情報
	 * @throws UsernameNotFoundException 指定ユーザーがいないエラー
	 */
	@Transactional // 認証は認証の間でのトランザクション管理設定にする
	public UserDetails loadUserByToken(final String token) throws BadTokenException {
		// 前提条件
		if (MyStringUtils.isEmpty(token)) throw new BadTokenException(); // JWTの指定なし

		// TODO: エラーパターン分けるか... JWTない、ユーザーいない、期限切れなど
		// 指定 JWTを複合化し 付け加えた情報(login ID)を解析し 解析したユーザー識別子からユーザー情報を検索し 認可ユーザー情報 へ変換する
		return this.jwtFactory.decodedJWT(token) // 指定 JWTを複合化
				.map(DecodedJWT::getSubject) // ユーザー識別子を解析し
				.map(UserId::of) // ユーザー識別子の型へ変換
				.flatMap(this.repository::findById) // ユーザー識別子 に該当するユーザー情報を検索し
				.map(this.userDetailsFactory::createOf) //認可ユーザー情報 へ変換する
				.orElseThrow(BadTokenException::new); // 見つからない場合などは JWTの値不正エラー とする
	}

	/**
	 * 指定メアド存在判定
	 * @param email メアド
	 * @return true: 存在 / false: それ以外
	 */
	public boolean existEmail(final String email) {
		// 前提条件
		if (MyStringUtils.isEmpty(email)) return false; // 空 TODO: 404
		if (Email.of(email).isEmpty()) return false; // 形式不正 TODO: 404

		// メアド検索 → 値存在
		return repository.findByEmail(Email.of(email)).isPresent();
	}
	/*********************************************
	 * 外部参照可能関数 (Domain Service)
	 *********************************************/
	/**
	 * @return アクセスユーザー取得 (要: ログイン済み)
	 */
	public User getCurrentUser() {
		// 認証されたユーザー情報から ユーザ識別子を取得し ユーザー識別子にて検索したユーザー情報(Entity)を User typeにして返却
		return Optional
				.ofNullable(SecurityContextHolder.getContext()) // 認証されたユーザー情報から
				.map(SecurityContext::getAuthentication) // 認証されたユーザーの結果 取得
				.map(Authentication::getName) // UserDetailsのgetName = 認証ユーザー識別子(userId)を取得し
				.map(UserId::of) // ユーザー識別子(userId)を独自の型にして
				.flatMap(this.repository::findById) // ユーザー識別子にて検索
				.map(this.userFactory::createOf) // 検索したユーザー情報(Entity)を User typeにして
				// TODO: throw JWT セッションタイムアウト的な 401?
				.orElseThrow(() -> new ResponseStatusException(HttpStatus.UNAUTHORIZED)); // なし = エラー(再認証もとむ)
	}
	/**
	 * JWTのエンコード(生成)
	 * @param user 生成するユーザー
	 * @return 生成するユーザーの JWT(文字列)
	 */
	public String encodeJWT(final User user) { return jwtFactory.encodeJWT(user); }
}
