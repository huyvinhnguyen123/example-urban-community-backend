package vn.urban.vietnam.urbancommunity.type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import vn.urban.vietnam.urbancommunity.model.value.CreatedAt;
import vn.urban.vietnam.urbancommunity.model.value.MessageId;
import vn.urban.vietnam.urbancommunity.model.value.UpdatedAt;

import java.io.Serializable;

/**
 * GraphQL output interface メッセージ
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Message implements Serializable {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** メッセージ識別子 */
	private MessageId id;
	/** 送信した部屋 */
	@Setter
	private Group group;
	/** 送信者 */
	@Setter
	private Profile sender;
	/** 送信メッセージ */
	private String messageText;
	/** 添付ファイルなど */
	@Setter
	private Attachment attachment;
	/** 登録日時(初回送信日時) */
	private CreatedAt createdAt;
	/** 更新日時 */
	private UpdatedAt updatedAt;
}
