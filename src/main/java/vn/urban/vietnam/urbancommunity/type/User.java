package vn.urban.vietnam.urbancommunity.type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.model.value.Email;
import vn.urban.vietnam.urbancommunity.model.value.UserId;

import java.io.Serializable;

/**
 * GraphQL output interface ユーザー
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** ユーザーID */
	private UserId id;
	/** メールアドレス */
	private Email email;
}
