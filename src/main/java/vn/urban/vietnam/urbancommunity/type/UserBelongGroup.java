package vn.urban.vietnam.urbancommunity.type;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.infrastructure.entity.key.UserBelongGroupId;

import java.io.Serializable;

/**
 * ユーザーが所属するグループ
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserBelongGroup implements Serializable {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 識別子 */
	private UserBelongGroupId id;
	/** 所属ユーザープロフィール */
	private Profile profile;
	/** 所属グループ */
	private Group group;
}
