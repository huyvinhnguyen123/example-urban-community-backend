package vn.urban.vietnam.urbancommunity.type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import vn.urban.vietnam.urbancommunity.model.value.Phone;
import vn.urban.vietnam.urbancommunity.model.value.UserId;
import vn.urban.vietnam.urbancommunity.model.value.UserName;

import java.io.Serializable;

/**
 * GraphQL output interface プロフィール
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Profile implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** 識別子 = ユーザーID */
	@NotNull
	private UserId id;
	/** ユーザー情報 */
	@NotNull
	private User user;
	/** ユーザー アバター */
	private Avatar avatar;
	/** ユーザー名 */
	@NotNull
	private UserName userName;
	/** 苗字 */
	private String lastName;
	/** 名前 */
	private String firstName;
	/** 住所 */
	private String location;
	/** 所属会社 */
	private String belongs;
	/** 電話番号 */
	private Phone phone;
}
