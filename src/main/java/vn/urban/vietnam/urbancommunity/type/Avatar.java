package vn.urban.vietnam.urbancommunity.type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import vn.urban.vietnam.urbancommunity.model.value.AvatarId;
import vn.urban.vietnam.urbancommunity.model.value.AvatarURL;

import java.io.Serializable;

/**
 * GraphQL output interface アバター(アイコン)
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Avatar implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** アバターID */
	private AvatarId id;
	/** アバターURL */
	private AvatarURL avatarURL;
}
