package vn.urban.vietnam.urbancommunity.type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;

/**
 * メッセージ追加
 */
@Getter
@Builder
@AllArgsConstructor
public class AppendMessageInput {
	/*********************************************
	 * Filed
	 *********************************************/
	/** 送信した部屋  = どこ宛 */
	@NotNull
	private final GroupId groupId;
	/** 本文 (null:なし 画像のみ可) */
	@Nullable
	private final String messageText;
}
