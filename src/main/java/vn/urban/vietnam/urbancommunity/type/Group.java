package vn.urban.vietnam.urbancommunity.type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import vn.urban.vietnam.urbancommunity.model.value.GroupId;

import java.io.Serializable;

/**
 * GraphQL output interface グループ
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Group implements Serializable {
	/*********************************************
	 * Filed
	 *********************************************/
	/** グループ識別子 */
	@NotNull
	private GroupId id;
	/** グループ名 */
	@NotNull
	private String groupName;
	/** グループ アバター */
	private Avatar avatar;
}
