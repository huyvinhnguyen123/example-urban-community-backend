package vn.urban.vietnam.urbancommunity.type;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import vn.urban.vietnam.urbancommunity.model.value.MessageId;

/**
 * GraphQL output interface 添付ファイル
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Attachment {
	/*********************************************
	 * フィールド
	 *********************************************/
	/** 添付メッセージID */
	@NotNull
	private MessageId id;
	/** なんだっけw */
	private String thumbURL;
	/** 添付ファイルURL */
	private String fileURL;
}
