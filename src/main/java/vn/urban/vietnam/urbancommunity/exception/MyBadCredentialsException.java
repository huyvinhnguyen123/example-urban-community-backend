package vn.urban.vietnam.urbancommunity.exception;

import org.springframework.security.authentication.BadCredentialsException;

/**
 * 独自 ID・PW認証 エラー
 * ユーザが存在しない、パスワードが間違ってるなど
 */
public class MyBadCredentialsException extends BadCredentialsException {
	/*********************************************
	 * Constructor
	 *********************************************/
	public MyBadCredentialsException() { super("メールアドレスまたはパスワードに誤りがあります。"); } // 何が間違っているかは教えない
}
