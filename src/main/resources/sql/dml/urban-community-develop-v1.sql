-- 綺麗さっぱりにしてからDML実行
TRUNCATE TABLE urban_community_user CASCADE;
-- passwordは暗号化結果 query passwordEncode で試して
-- TODO: message_id 以外 IDの形式 特に決めていない
INSERT INTO urban_community_user (user_id, email, password) VALUES
 ('User:0000000001', 'yhiguchi@urban-web.co.jp', '$2a$10$pKFQ/JoRiq0q87.eoMQfium8ZbkDn0QAa2S9s7f1bXfBwpQxXAXOm')
 , ('User:0000000002', 'tkojima@urban-web.co.jp', '$2a$10$pKFQ/JoRiq0q87.eoMQfium8ZbkDn0QAa2S9s7f1bXfBwpQxXAXOm')
;
TRUNCATE TABLE avatar CASCADE;
INSERT INTO avatar (avatar_id, avatar_url) VALUES
 ('Avatar:0000000001', 'https://www.dropbox.com/s/65ffn58vqe9eohx/test.jpg?raw=1')
 , ('Avatar:0000000002', 'http://urban-web.co.jp/wp/wp-content/uploads/2020/11/logo2020.png')
;
TRUNCATE TABLE profile CASCADE;
INSERT INTO profile (user_id, avatar_id, user_name, first_name, last_name, belongs, location, phone) VALUES
 ('User:0000000001', 'Avatar:0000000001', 'yhigu', '陽祐', '樋口', 'URBAN Corp Inc', '東京', NULL)
 , ('User:0000000002', NULL, 'tkojima', '聖加', '小島', 'URBAN Corp Inc', '神奈川', NULL)
;
TRUNCATE TABLE friend CASCADE;
INSERT INTO friend (user_id, friend_user_id) VALUES
 ('User:0000000001', 'User:0000000002')
 , ('User:0000000002', 'User:0000000001')
;
TRUNCATE TABLE friend_request CASCADE;
INSERT INTO friend_request (user_id, receiver_id, accepted) VALUES
 ('User:0000000001', 'User:0000000002', true)
;
TRUNCATE TABLE urban_community_group CASCADE;
INSERT INTO urban_community_group (group_id, avatar_id, group_name) VALUES
 ('Group:0000000001', 'Avatar:0000000002', 'URBAN Corp')
;
TRUNCATE TABLE user_belong_group CASCADE;
INSERT INTO user_belong_group (user_id, group_id) VALUES
 ('User:0000000001', 'Group:0000000001')
 , ('User:0000000002', 'Group:0000000001')
;
-- message_id は採番だるそうなのでランダム+ms秒文字
TRUNCATE TABLE message CASCADE;
INSERT INTO message (group_id, message_id, message_text, sender_id) VALUES
 ('Group:0000000001', 's2f3d20220919145323498', 'Hello World!! (message)', 'User:0000000001')
;
TRUNCATE TABLE seen_message CASCADE;
INSERT INTO seen_message (user_id, group_id, message_id, saw) VALUES
 ('User:0000000001', 'Group:0000000001', 's2f3d20220919145323498', true)
,  ('User:0000000002', 'Group:0000000001', 's2f3d20220919145323498', false)
;
TRUNCATE TABLE attachment CASCADE;
-- attachment のファイルサーバー要件に従うので今後
