-- まずは削除
DROP TABLE IF EXISTS "avatar" CASCADE;
DROP TABLE IF EXISTS "attachment" CASCADE;
DROP TABLE IF EXISTS "seen_message" CASCADE;
DROP TABLE IF EXISTS "message" CASCADE;
DROP TABLE IF EXISTS "user_belong_group" CASCADE;
DROP TABLE IF EXISTS "urban_community_group" CASCADE;
DROP TABLE IF EXISTS "friend_request" CASCADE;
DROP TABLE IF EXISTS "friend" CASCADE;
DROP TABLE IF EXISTS "profile" CASCADE;
DROP TABLE IF EXISTS "urban_community_user" CASCADE;

-- ユーザー PostgreSQLのため "user" は使えない
CREATE TABLE "urban_community_user" (
  "user_id" varchar PRIMARY KEY, -- キー
  "email" varchar NOT NULL UNIQUE, -- メアド (ログインID)
  "password" varchar NOT NULL, -- パスワード
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 登録日時
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP) -- 更新日時
);
-- プロフィール
CREATE TABLE "profile" (
  "user_id" varchar PRIMARY KEY, -- キー = ユーザーごと
  "avatar_id" varchar, -- 外部参照キー = アバター (未設定可)
  "user_name" varchar NOT NULL UNIQUE, -- ユーザー名
  "first_name" varchar, -- 名
  "last_name" varchar, -- 姓
  "belongs" varchar, -- 所属会社等
  "location" varchar, -- 住所等
  "phone" varchar, -- 電話番号(ベトナムの形式わからんから 一旦フィールドわけない)
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 登録日時
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP) -- 更新日時
);
-- アバター (ユーザー または グループ)
CREATE TABLE "avatar" (
  "avatar_id" varchar PRIMARY KEY, -- キー = アバターごと
  "avatar_url" varchar NOT NULL, -- アバター保存先サーバーからのダウンロードURL
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 登録日時
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP) -- 更新日時
);
-- ユーザー 連絡先(友だち一覧)
CREATE TABLE "friend" (
  "user_id" varchar,
  "friend_user_id" varchar, -- 友だちのUser Id
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 登録日時
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 更新日時
  PRIMARY KEY ("user_id", "friend_user_id") -- 複合キー = ユーザーの友だちごと
);
-- ユーザー 連絡先 申請一覧
CREATE TABLE "friend_request" (
  "user_id" varchar, -- (申請者の)ユーザー
  "receiver_id" varchar, -- 申請先のユーザー
  "accepted" boolean NOT NULL, -- true: 承認(削除可:多重送信防止用) / false: 未承認(削除不可)
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 登録日時
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 更新日時
  PRIMARY KEY ("user_id", "receiver_id") -- 複合キー = ユーザーの友だちリクエストごと
);
-- グループ (トークルーム単位) GROUP byの groupと重複する可能性を考慮し 名称変更
CREATE TABLE "urban_community_group" (
  "group_id" varchar PRIMARY KEY, -- グループ
  "avatar_id" varchar, -- グループのアバター (未設定可)
  "group_name" varchar, -- グループ名
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 登録日時
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP) -- 更新日時
);
-- ユーザー所属グループ (トークルーム単位)
CREATE TABLE "user_belong_group" (
  "user_id" varchar,
  "group_id" varchar,
--   "accepted" boolean NOT NULL, -- true: グループ管理者承認済 / false: グループ未承認
  PRIMARY KEY ("user_id", "group_id") -- 複合キー = ユーザーの所属グループごと
);
-- (グループの)メッセージ
CREATE TABLE "message" (
  "group_id" varchar,
  "message_id" varchar,
  "message_text" text, -- 本文 (本文なし可) (改行可)
  "sender_id" varchar NOT NULL, -- 送信者 (ユーザーID)
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 登録日時
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 更新日時
  PRIMARY KEY ("group_id", "message_id") -- 複合キー = グループのメッセージごと
);
-- ユーザーの(グループの)メッセージ 閲覧履歴 (TODO:量が所属メンバー分だけ増えるのでバッチ削除検討)
CREATE TABLE "seen_message" (
  "user_id" varchar,
  "message_id" varchar,
  "group_id" varchar,
  "saw" boolean NOT NULL, -- true: ユーザー閲覧 / false: 未読
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 登録日時
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 更新日時
  PRIMARY KEY ("user_id", "message_id", "group_id") -- 複合キー = ユーザーのグループのメッセージごと
);
--  (グループの)メッセージの添付 TODO: ファイルサーバー要件により変更
CREATE TABLE "attachment" (
  "group_id" varchar,
  "message_id" varchar,
  -- ↓ TODO: ファイルサーバー要件により変更
  "thumb_url" varchar, -- 画像のファイル ... だった気がするw
  "file_url" varchar, -- 画像以外のファイル ... だった気がするw
  -- ↑ TODO: ファイルサーバー要件により変更
  "created_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 登録日時
  "updated_at" timestamp DEFAULT (CURRENT_TIMESTAMP), -- 更新日時
  PRIMARY KEY ("group_id", "message_id") -- 複合キー = グループのメッセージごと (必須ではない、1メッセージ 1ファイル)
);

-- 外部キー
-- ユーザー参照
ALTER TABLE "profile" ADD FOREIGN KEY ("user_id") REFERENCES "urban_community_user" ("user_id");
ALTER TABLE "friend" ADD FOREIGN KEY ("user_id") REFERENCES "urban_community_user" ("user_id");
ALTER TABLE "friend_request" ADD FOREIGN KEY ("user_id") REFERENCES "urban_community_user" ("user_id");
ALTER TABLE "user_belong_group" ADD FOREIGN KEY ("user_id") REFERENCES "urban_community_user" ("user_id");
ALTER TABLE "seen_message" ADD FOREIGN KEY ("user_id") REFERENCES "urban_community_user" ("user_id");
-- アバター参照
ALTER TABLE "profile" ADD FOREIGN KEY ("avatar_id") REFERENCES "avatar" ("avatar_id");
ALTER TABLE "urban_community_group" ADD FOREIGN KEY ("avatar_id") REFERENCES "avatar" ("avatar_id");
-- グループ参照
ALTER TABLE "user_belong_group" ADD FOREIGN KEY ("group_id") REFERENCES "urban_community_group" ("group_id");
ALTER TABLE "message" ADD FOREIGN KEY ("group_id") REFERENCES "urban_community_group" ("group_id");
-- メッセージ参照
ALTER TABLE "attachment" ADD FOREIGN KEY ("group_id", "message_id") REFERENCES "message" ("group_id", "message_id");
ALTER TABLE "seen_message" ADD FOREIGN KEY ("group_id", "message_id") REFERENCES "message" ("group_id", "message_id");
